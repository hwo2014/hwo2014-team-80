
#pragma once

#include "hwo_model/car.hpp"
#include "hwo_model/world.hpp"

namespace hwo {
	class Controller {
	public:
		virtual void think(const hwo::model::World& world, int carIndex, hwo::model::Car::Controls& controls) = 0;
	};
}