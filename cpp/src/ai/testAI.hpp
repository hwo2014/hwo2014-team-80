
#pragma once

#include "ai/ai.hpp"
#include "hwo_model/simulator.hpp"

class TestController : public hwo::Controller {
	
	bool directionsInited;
	float slownessDebugFactor;
	int turboSegment;
public:

	std::vector<std::vector<hwo::model::LaneChoice>> directions;

	TestController(float slownessFactor=1.0f) : slownessDebugFactor(slownessFactor) {
		directionsInited = false;
	}

	struct Plan {
		enum Behaviour {
			RAM = 0,
			DODGE = 1
		};

		Plan(const hwo::model::CarState& target, Behaviour behaviour) : target(target), behaviour(behaviour) {
		}

		const hwo::model::CarState& target;
		Behaviour behaviour;
	};

	std::vector<Plan> evilPlans;

	void findPlans(const hwo::model::World& world, int carIndex) {
		// if there's an enemy pretty close ahead
		const auto& cars = world.getCars();
		const auto& myCar = world.getMyCar();

		turboSegment = hwo::model::TrackAlgorithms::longestStraight(world);
		evilPlans.clear();

		hwo::model::NearestEnemy enemy = hwo::model::TrackAlgorithms::distanceToNextHostile(world, directions);
		if(enemy.distance > 100000.0f) // coast is clear, we can just go cruising.
			return;

		float distanceToBend = hwo::model::TrackAlgorithms::distanceToNextBend(world, directions);
		float distanceToSwitch = hwo::model::TrackAlgorithms::distanceToNextSwitch(world, directions);

		if(enemy.distance < distanceToBend) {
			// Maybe ramming possibility!
			float timeFrames = distanceToBend / (enemy.enemy.velocity + 0.1f);
			float myDistanceCovered = 0;
			if(myCar.state.isTurboAvailable()) {
				// check if we can ram with turbo.
				hwo::model::SwitchDir direction = hwo::model::SwitchDir::Forward;
				hwo::model::CarState myStateCopy = myCar.state;
				int tick = world.gameTick;
				int turboTicks = myCar.state.getAvailableTurbo().durationTicks;
				float turboMultiplier = myCar.state.getAvailableTurbo().turboFactor;
				
				for(int i=0; i<timeFrames; ++i) {
					myDistanceCovered += myStateCopy.velocity;
					if(--turboTicks <= 0)
						turboMultiplier = 1;
					hwo::model::Simulator::simulateOneTick(world, myStateCopy, direction, turboMultiplier, ++tick);
				}

				if(myDistanceCovered + 40 > distanceToBend) {
					// yes, we can RAM.
					evilPlans.push_back(Plan(world.getCar(enemy.colorName).state, Plan::RAM));
					return;
				}
			}
			else if(myCar.state.isTurboActive(world.gameTick)) {
				// Maybe we are already ramming.
				// check if we can ram with turbo.
				hwo::model::SwitchDir direction = hwo::model::SwitchDir::Forward;
				hwo::model::CarState myStateCopy = myCar.state;
				int tick = world.gameTick;
				int turboTicks = myCar.state.getActiveTurbo().durationTicks;
				float turboMultiplier = myCar.state.getActiveTurbo().turboFactor;
				
				for(int i=0; i<timeFrames; ++i) {
					myDistanceCovered += myStateCopy.velocity;
					hwo::model::Simulator::simulateOneTick(world, myStateCopy, direction, 1.0f, ++tick);
				}

				if(myDistanceCovered + 40 > distanceToBend) {
					// yes, we can RAM.
					evilPlans.push_back(Plan(world.getCar(enemy.colorName).state, Plan::RAM));
					return;
				}
			}
			
			// check if we can ram without turbo?
		}
		
		{
			const auto& car = world.getCar(enemy.colorName);
			// check if we should try to dodge this dude.
			if(enemy.distance < 200.0f && car.state.velocity < hwo::model::Simulator::maxCurrentVelocity(world, car, directions) * 0.80f && car.state.acceleration < 0.85f * world.physicsModel.guessAcceleration(1.0f, car.state.velocity, car.state.angle, 1.0f)) {
				// enemy car is slow bastard? maybe we should try finding a different lane than him.
				evilPlans.push_back(Plan(car.state, Plan::DODGE));
			}
		}
	}

	hwo::model::SwitchDir getDodgeDirection(const hwo::model::World& world, const hwo::model::CarState& myState, const hwo::model::CarState& targetState) {
		
		int myCurrentLane = myState.switchingToLane;

		float leftDodgeScore = 0;
		float forwardDodgeScore = hwo::model::TrackAlgorithms::distanceToNextHostile(world, myCurrentLane).distance;
		float rightDodgeScore = 0;
		
		if(myCurrentLane > 0) {
			leftDodgeScore = hwo::model::TrackAlgorithms::distanceToNextHostile(world, myCurrentLane - 1).distance;
		}
		if(myCurrentLane < (int)world.getLaneOffsets().size()-1) {
			rightDodgeScore = hwo::model::TrackAlgorithms::distanceToNextHostile(world, myCurrentLane + 1).distance;
		}

		if(leftDodgeScore > rightDodgeScore) {
			if(leftDodgeScore > forwardDodgeScore) {
				return hwo::model::SwitchDir::Left;
			}
			return hwo::model::SwitchDir::Forward;
		}
		else {
			if(rightDodgeScore > forwardDodgeScore) {
				return hwo::model::SwitchDir::Right;
			}
			return hwo::model::SwitchDir::Forward;
		}

		ASSERT(false, "should not reach this");
		return hwo::model::SwitchDir::Forward;
	}


	float getThrottleToNextTightCorner(const hwo::model::World& world, const hwo::model::CarState& car, float speedModifier) 
	{
		auto& track = world.getTrack();
		float distance = world.getTrackPieceLength(car.activeSegment, car.activeLane, car.getCurrentSwitchDir()) - car.activeSegmentPosition;
		float minSafeThrottle = 1;
		float currentRadius = world.getTrackPieceRadius(car.activeSegment, car.activeLane, car.getCurrentSwitchDir());
		int activeLane = car.switchingToLane;

		for (int i = 1; i < 10; i++)
		{
			int pieceIndex = (car.activeSegment+i) % track.size();
			float radius = world.getTrackPieceRadius(pieceIndex, activeLane, hwo::model::SwitchDir::Forward);
			
			if( (currentRadius == 0 && radius > 0) || (radius != 0 && radius < currentRadius) )
			{
				float safeSpeed =  world.physicsModel.guessSafeSpeed(40, radius);
				float safeThrottle = hwo::model::Simulator::getThrottleForVelocity(world, car, speedModifier * safeSpeed, distance);
				return safeThrottle;
			}

			distance += world.getTrackPieceLength(pieceIndex, activeLane , hwo::model::Forward);
		}
		return minSafeThrottle;
	}

	float getThrottle(const hwo::model::World& world, hwo::model::Car::Controls& controls, const hwo::model::CarState& car, hwo::model::SwitchDir nextDirection) 
	{
		float throttle = hwo::model::Simulator::getSafeThrottle(world, car, nextDirection);
		if (!world.physicsModel.isInited() && !world.track[car.activeSegment].isStraight()) 
		{
			throttle *= 0.8f; //avoid crashing when collecting early samples to keep samples cleaner
		}

		if (throttle < 0.1f && car.velocity < 2)
		{
			controls.badModel = true;
			throttle = 0.4f;
		}

		if (car.lap == world.sessionInfo.laps-1)
		{
			bool allStraight = true;
			for (unsigned i = car.activeSegment; i < world.track.size(); i++)
			{
				if (!world.track[i].isStraight())
				{
					allStraight = false;
					break;
				}
			}
			if (allStraight) 
			{
				throttle = 1;
				LOG("Full throttle to finish line!!!");
			}
		}

		return throttle * slownessDebugFactor;
	}

	virtual void think(const hwo::model::World& world, int carIndex, hwo::model::Car::Controls& controls) override {

		if(!directionsInited) {
			directionsInited = true;
			directions = hwo::model::TrackAlgorithms::buildShortestPaths(world.getLaneOffsets(), world.getTrack());
		}

		findPlans(world, carIndex);
	
		if(evilPlans.size()) {
			if(evilPlans[0].behaviour == Plan::RAM) {
				const auto& myState = world.getMyCar().state;
				
				int nextTrackPieceIndex = (myState.activeSegment + 1) % world.getTrack().size();
				auto direction = directions[myState.activeLane][nextTrackPieceIndex].switchDirection;
				controls.gas = 1.0f; // RAMMING SPEED
				controls.switchLane = direction;

				// And follow our natural line
				const auto& carObject = world.getCar(carIndex);
				const auto& car = carObject.state;

				if(world.getMyCar().state.isTurboAvailable() && direction == hwo::model::SwitchDir::Forward)
					controls.turboBoost = true; // RAM RAM RAM
				return;
			}
			else {
				// ok there's some slow dude in front of us, and we need to dodge him.
				const auto& myCar = world.getMyCar();
				const auto& myState = myCar.state;
				int nextSegment = (myState.activeSegment + 1) % world.getTrack().size();
				if(world.getTrack()[nextSegment].hasSwitch) {
					// distance left in my current segment
					float distance = world.getTrack()[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(myState.switchingToLane - myState.activeLane));
					distance -= myCar.state.activeSegmentPosition;
					if(distance / myState.velocity < 4) {
						// do it.
						controls.switchLane = getDodgeDirection(world, myState, evilPlans[0].target);
						controls.gas = getThrottle(world, controls, world.getCar(carIndex).state, controls.switchLane); 
						return;
					}
				}
			}
		}


		// if there's no special plans at hand, just drive fast.
		const auto& myCar = world.getCar(carIndex);
		const auto& myCarState = myCar.state;
		auto direction = directions[myCarState.activeLane][(myCarState.activeSegment + 1) % world.getTrack().size()].switchDirection;
		
		controls.gas = getThrottle(world, controls, myCarState, direction);

		// if we could be driving a lot faster, we need more acceleration = use turbo.
		controls.turboBoost = false;
		float maximumSurvivableVelocity = hwo::model::Simulator::maxCurrentVelocity(world, myCar, directions);
		bool turboAllowed = (((myCarState.activeSegment + 1) % world.getTrack().size()) == turboSegment);
		turboAllowed |= (myCarState.activeSegment  == turboSegment);
		if(myCarState.velocity < maximumSurvivableVelocity * 0.75f && turboAllowed) {
			if(myCar.state.isTurboAvailable()) {
				controls.turboBoost = true;
			}
		}
		
		const auto& carObject = world.getCar(carIndex);
		const auto& car = carObject.state;

		float currentSegmentLength = world.getTrack()[car.activeSegment].getLength(world.getLaneOffsets(), car.activeLane);
		float currentSegmentProgress = car.activeSegmentPosition;
		float unfinished = currentSegmentLength - currentSegmentProgress;

		if(unfinished / car.velocity < 3) {
			int nextTrackPieceIndex = (car.activeSegment + 1) % world.getTrack().size();
			auto direction = directions[car.activeLane][nextTrackPieceIndex].switchDirection;
			controls.switchLane = direction;
		}
		else {
			controls.switchLane = hwo::model::SwitchDir::Forward; // don't tell the server our direction until have to.
		}

		return;
	}
};
