
#pragma once


#include "ai/ai.hpp"
#include "hwo_model/switchdir.hpp"
#include "hwo_model/trackAlgorithms.hpp"

#include <vector>

class JBotController : public hwo::Controller {
	

	bool directionsInited;
public:
	std::vector<std::vector<hwo::model::LaneChoice>> directions;
	JBotController() {
		directionsInited = false;
	}

		struct Plan {
		enum Behaviour {
			RAM = 0,
			DODGE = 1
		};

		Plan(const hwo::model::CarState& target, Behaviour behaviour) : target(target), behaviour(behaviour) {
		}

		const hwo::model::CarState& target;
		Behaviour behaviour;
	};

	std::vector<Plan> evilPlans;

	void findPlans(const hwo::model::World& world, int carIndex) {
		// if there's an enemy pretty close ahead
		const auto& cars = world.getCars();
		const auto& myCar = world.getMyCar();

		evilPlans.clear();

		hwo::model::NearestEnemy enemy = hwo::model::TrackAlgorithms::distanceToNextHostile(world, directions);
		if(enemy.distance > 100000.0f) // coast is clear, we can just go cruising.
			return;

		float distanceToBend = hwo::model::TrackAlgorithms::distanceToNextBend(world, directions);
		float distanceToSwitch = hwo::model::TrackAlgorithms::distanceToNextSwitch(world, directions);

		if(enemy.distance < distanceToBend && enemy.distance > 100.0f) {
			// Maybe ramming possibility!
			float timeFrames = distanceToBend / (enemy.enemy.velocity + 0.1f);
			float myDistanceCovered = 0;
			if(myCar.state.isTurboAvailable()) {
				// check if we can ram with turbo.
				hwo::model::SwitchDir direction = hwo::model::SwitchDir::Forward;
				hwo::model::CarState myStateCopy = myCar.state;
				int tick = world.gameTick;
				int turboTicks = myCar.state.getAvailableTurbo().durationTicks;
				float turboMultiplier = myCar.state.getAvailableTurbo().turboFactor;
				
				for(int i=0; i<timeFrames; ++i) {
					myDistanceCovered += myStateCopy.velocity;
					if(--turboTicks <= 0)
						turboMultiplier = 1;
					hwo::model::Simulator::simulateOneTick(world, myStateCopy, direction, turboMultiplier, ++tick);
				}

				if(myDistanceCovered > distanceToBend) {
					// yes, we can RAM.
					evilPlans.push_back(Plan(world.getCar(enemy.colorName).state, Plan::RAM));
					return;
				}
			}
			else if(myCar.state.isTurboActive(world.gameTick)) {
				// Maybe we are already ramming.
				// check if we can ram with turbo.
				hwo::model::SwitchDir direction = hwo::model::SwitchDir::Forward;
				hwo::model::CarState myStateCopy = myCar.state;
				int tick = world.gameTick;
				int turboTicks = myCar.state.getActiveTurbo().durationTicks;
				float turboMultiplier = myCar.state.getActiveTurbo().turboFactor;
				
				for(int i=0; i<timeFrames; ++i) {
					myDistanceCovered += myStateCopy.velocity;
					hwo::model::Simulator::simulateOneTick(world, myStateCopy, direction, 1.0f, ++tick);
				}

				if(myDistanceCovered > distanceToBend) {
					// yes, we can RAM.
					evilPlans.push_back(Plan(world.getCar(enemy.colorName).state, Plan::RAM));
					return;
				}
			}
			
			// check if we can ram without turbo?
		}
		
		{
			const auto& car = world.getCar(enemy.colorName);
			// check if we should try to dodge this dude.
			if(enemy.distance < 200.0f && car.state.velocity < hwo::model::Simulator::maxCurrentVelocity(world, car, directions) * 0.80f && car.state.acceleration < 0.85f * world.physicsModel.guessAcceleration(1.0f, car.state.velocity, car.state.angle, 1.0f)) {
				// enemy car is slow bastard? maybe we should try finding a different lane than him.
				evilPlans.push_back(Plan(car.state, Plan::DODGE));
			}
		}
	}

	hwo::model::SwitchDir getDodgeDirection(const hwo::model::World& world, const hwo::model::CarState& myState, const hwo::model::CarState& targetState) {
		int numLanes = world.getLaneOffsets().size();
		int forbiddenLane = targetState.switchingToLane;
		
		if(myState.switchingToLane > 0) {
			if(forbiddenLane != myState.switchingToLane - 1) {
				// Left is an option
				return hwo::model::SwitchDir::Left;
			}
		}

		if(myState.switchingToLane < numLanes - 1) {
			if(forbiddenLane != myState.switchingToLane + 1) {
				// Right is an option
				return hwo::model::SwitchDir::Right;
			}
		}
		
		if(forbiddenLane != myState.switchingToLane) {
			// Forward is an option
			return hwo::model::SwitchDir::Forward;
		}

		ASSERT(false, "should not reach this");
		return hwo::model::SwitchDir::Forward;
	}

	
	/*
	float optimizeDistance(const hwo::model::World& world, hwo::model::CarState& car, int depth) 
	{
		if (car.dead)
			return -1;
		if (depth == 0 ) 
		{
			return car.getRacePosition();	
		}
		else 
		{
			hwo::model::CarState testcar(car);
			float throttleLevel = 1;
			float bestDistance = -1;
			for (int i = 0; i < 5; i++) 
			{
				world.simulate(testcar, throttleLevel);
				float dist = optimizeDistance(world, testcar, depth-1);
				if (dist > bestDistance) 
				{
					bestDistance = dist;
				}
				throttleLevel -= 0.2f;
				testcar = car; //revert simulation
			}
			return bestDistance;
		}
	}
	*/

	
	float getBestThrottle(const hwo::model::World& world, hwo::model::CarState& car) 
	{
		float throttleLevel = 1;
		float bestThrottleLevel = 0;
		float bestDistance = -1;

		for (int i = 0; i < 100; i++) 
		{
			if(hwo::model::Simulator::isSafeThrottle(world, car, hwo::model::SwitchDir::Forward, throttleLevel)) 
			{
				bestThrottleLevel = throttleLevel;
				break;
			}
			
			throttleLevel -= 0.01f;
		}

		return bestThrottleLevel;
	}
	
	float getSafeSpeedBestThrottle(const hwo::model::World& world, hwo::model::CarState& car) 
	{

		float distance = 0;
		float minSafeThrottle = 1;
		int activeLane = car.activeLane;
		{
			float radius = world.getTrackPieceRadius(car.activeSegment, car.activeLane, car.getCurrentSwitchDir());
			float safeSpeed =  world.physicsModel.guessSafeSpeed(45, radius);

			float safeThrottle = hwo::model::Simulator::getThrottleForVelocity(world, car, safeSpeed, distance);

			minSafeThrottle = safeThrottle;
			distance = world.getTrackPieceLength(car.activeSegment, car.activeLane, car.getCurrentSwitchDir()) - car.activeSegmentPosition;
			activeLane = car.switchingToLane;
		}
		
		for (int i = 1; i < 10; i++) 
		{
			auto & track = world.getTrack();
			int pieceIndex = (car.activeSegment+i) % track.size();
			
			float radius = world.getTrackPieceRadius(pieceIndex, activeLane, hwo::model::Forward);
			float safeSpeed =  world.physicsModel.guessSafeSpeed(40, radius);

			float safeThrottle = hwo::model::Simulator::getThrottleForVelocity(world, car, safeSpeed, distance);

			if (safeThrottle < minSafeThrottle) 
			{
				minSafeThrottle = safeThrottle;
			}

			distance += world.getTrackPieceLength(pieceIndex, activeLane , hwo::model::Forward);
		}


		

		return minSafeThrottle;
	}

	//find the next piece with tighter radius than current piece and calculate best throttle to reach safe speed on that piece
	float getThrottleToNextTightCorner(const hwo::model::World& world, hwo::model::CarState& car, float speedModifier) 
	{
		float distance = 0;
		float minSafeThrottle = 1;
		float currentRadius = 0;
		int activeLane = car.activeLane;
		float currentAngle = 0;
		{
			float radius = world.getTrackPieceRadius(car.activeSegment, car.activeLane, car.getCurrentSwitchDir());
			currentRadius = radius;
			distance = world.getTrackPieceLength(car.activeSegment, car.activeLane, car.getCurrentSwitchDir()) - car.activeSegmentPosition;
			currentAngle = world.getTrack()[car.activeSegment].arcedAngle;
			activeLane = car.switchingToLane;
		}

		for (int i = 1; i < 10; i++) 
		{
			auto & track = world.getTrack();
			int pieceIndex = (car.activeSegment+i) % track.size();
			//TODO do not break for corners after finish line
			
			

			float radius = world.getTrackPieceRadius(pieceIndex, activeLane, hwo::model::Forward);
			float angle = currentAngle = world.getTrack()[pieceIndex].arcedAngle;
			if ((currentRadius == 0 && radius > 0) || (radius != 0 && radius < currentRadius) || (radius != 0 && radius == currentRadius && currentAngle*angle < 0)) 
			{
				float safeSpeed =  world.physicsModel.guessSafeSpeed(40, radius);
				float safeThrottle = hwo::model::Simulator::getThrottleForVelocity(world, car, speedModifier*safeSpeed, distance);
				return safeThrottle;
			}

			distance += world.getTrackPieceLength(pieceIndex, activeLane , hwo::model::Forward);
			
		}
		return minSafeThrottle;
	}



	void learningControl(const hwo::model::World& world, int carIndex, hwo::model::Car::Controls& controls) 
	{
		const auto& track = world.getTrack();
		const auto& carObject = world.getCar(carIndex);
		const auto& car = carObject.state;
		
		const auto& segment = track[(car.activeSegment + 1) % track.size()];
		float activeSegmentRadius = track[car.activeSegment].getRadius(world.getLaneOffsets()[car.activeLane]);
		float nextSegmentRadius = segment.getRadius(world.getLaneOffsets()[car.activeLane]);
		const auto& futureSegment = track[(car.activeSegment + 2) % track.size()];
		float futureSegmentRadius = futureSegment.getRadius(world.getLaneOffsets()[car.activeLane]);

		bool careful = false;
		float maxGas = 1.00f;
			
		if(activeSegmentRadius < 200) {
			if(std::abs(car.angle) > 38 && car.angle * car.deltaAngle > 0 || std::abs(car.deltaAngle) > 2) {
				maxGas = 0.0f;
			}
			else if(car.angle * car.deltaAngle > 80) {
				maxGas = 0.20f;
			}
			else if(car.angle * car.deltaAngle > 55) {
				maxGas = 0.40f;
			}
			else if(car.angle * car.deltaAngle > 35) {
				maxGas = 0.50f;
			}
			else if(car.angle * car.deltaAngle > 20) {
				maxGas = 0.65f;
			}
			else if(std::abs(car.angle) > 1 && car.angle * car.deltaAngle > 1) {
				maxGas = 0.90f;
			}
		}
		else if(car.velocity > nextSegmentRadius / 15.0f) {
			maxGas = 0.0f;
		}
		else if(car.velocity > futureSegmentRadius / 15.0f) {
			maxGas = 0.30f;
		}

		controls.gas = maxGas;
	}

	virtual void think(const hwo::model::World& world, int carIndex, hwo::model::Car::Controls& controls) override {

		const hwo::model::Car& realCar = world.getCar(carIndex);
		hwo::model::CarState car = realCar.state;

		if(!directionsInited) {
			directionsInited = true;
			directions = hwo::model::TrackAlgorithms::buildShortestPaths(world.getLaneOffsets(), world.getTrack());
		}

		findPlans(world, carIndex);

		if(evilPlans.size()) {
			if(evilPlans[0].behaviour == Plan::RAM) {
				const auto& myState = world.getMyCar().state;
				int nextTrackPieceIndex = (myState.activeSegment + 1) % world.getTrack().size();
				auto direction = directions[myState.activeLane][nextTrackPieceIndex].switchDirection;
				int plannedLane = myState.activeLane + direction;

				if(plannedLane == evilPlans[0].target.switchingToLane) {
					controls.gas = 1.0f; // RAMMING SPEED
					
					// And follow our natural line
					const auto& carObject = world.getCar(carIndex);
					const auto& car = carObject.state;

					int nextTrackPieceIndex = (car.activeSegment + 1) % world.getTrack().size();
					auto direction = directions[car.activeLane][nextTrackPieceIndex].switchDirection;
					controls.switchLane = direction;

					if(world.getMyCar().state.isTurboAvailable())
						controls.turboBoost = true; // RAM RAM RAM
					return;
				}
				else {
					// revert to ordinary driving?
				}
			}
			else {
				// ok there's some slow dude in front of us, and we need to dodge him.
				const auto& myCar = world.getMyCar();
				const auto& myState = myCar.state;
				int nextSegment = (myState.activeSegment + 1) % world.getTrack().size();
				if(world.getTrack()[nextSegment].hasSwitch) {
					// distance left in my current segment
					float distance = world.getTrack()[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(myState.switchingToLane - myState.activeLane));
					distance -= myCar.state.activeSegmentPosition;
					if(distance / myState.velocity < 4) {
						// do it.
						controls.switchLane = getDodgeDirection(world, myState, evilPlans[0].target);
						controls.gas = hwo::model::Simulator::getSafeThrottle(world, world.getCar(carIndex).state, controls.switchLane);
						return;
					}
				}
			}
		}



		const auto& track = world.getTrack();

		//if (world.gameTick < 200) 
		//{
		//	learningControl(world, carIndex, controls);
		//}
		//else 
		{
			float throttle1 =getBestThrottle(world, car); 
			float throttle2 =getSafeSpeedBestThrottle(world, car); 

			float maxSpeed = hwo::model::Simulator::maxCurrentVelocity(world, realCar, directions);
			float throttle3 = hwo::model::Simulator::getThrottleForVelocity(world, car, 0.8f*maxSpeed, 0);

			float throttle4 = getThrottleToNextTightCorner(world, car, 1.1f);

			//LOG("Throttle4? %d", (throttle4 < throttle1));

			controls.gas = throttle4 < throttle1? throttle4 : throttle1;
			
			//controls.gas = throttle2;
			/*
			float a = 3;
			float b = 1;
			float c = 1;
			controls.gas = (a*throttle1+b*throttle2+c*throttle3) / (a+b+c);
			*/

			
			
			

		}
		
		int nextTrackPieceIndex = (car.activeSegment + 1) % world.getTrack().size();
		auto direction = directions[car.activeLane][nextTrackPieceIndex].switchDirection;
		controls.switchLane = direction;


		float currentSegmentLength = world.getTrack()[car.activeSegment].getLength(world.getLaneOffsets(), car.activeLane);
		float currentSegmentProgress = car.activeSegmentPosition;
		float unfinished = currentSegmentLength - currentSegmentProgress;
		if(unfinished / car.velocity < 3) {
			int nextTrackPieceIndex = (car.activeSegment + 1) % world.getTrack().size();
			auto direction = directions[car.activeLane][nextTrackPieceIndex].switchDirection;
			controls.switchLane = direction;
		}
		else {
			controls.switchLane = hwo::model::SwitchDir::Forward; // don't tell the server our direction until have to.
		}

		if(car.isTurboAvailable() &&   track[(car.activeSegment + 2) % track.size()].isStraight()&&   track[(car.activeSegment + 1) % track.size()].isStraight()&&  track[(car.activeSegment) % track.size()].isStraight())
		{
			//todo find longest straight
			controls.turboBoost = true;
		}
	}
};
