
#pragma once

#include "ai/ai.hpp"
#include "input/userio.hpp"

class HumanController : public hwo::Controller {
	
	std::shared_ptr<hwo::UserIO> pUserIO;
	
public:
	HumanController(std::shared_ptr<hwo::UserIO> pUserIO) {
		this->pUserIO = pUserIO;
	}

	virtual void think(const hwo::model::World& world, int carIndex, hwo::model::Car::Controls& controls) override {
		if(pUserIO->isKeyDown('D')) {
			controls.gas = 0.0f;
		}
		if(pUserIO->isKeyDown('F')) {
			controls.gas = 0.5f;
		}
		if(pUserIO->isKeyDown('G')) {
			controls.gas = 1.0f;
		}
	}
};
