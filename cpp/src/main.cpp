
#include <iostream>
#include <jsoncons/json.hpp>
#include "samplebot/protocol.hpp"
#include "samplebot/connection.hpp"
#include "samplebot/game_logic.hpp"
#include "util/timer.hpp"

#ifndef FINAL_BUILD
#include "messagesystem/TypedMessage.hpp"
#include "properties/PropertyManager.hpp"
#include "thread/TaskManager.hpp"
#include "thread/fiber/Fiber.hpp"
#include "util/attributes.hpp"
#include "util/fps_manager.hpp"

#include "graphics/window/window.hpp"
#include "input/userio.hpp"

#include "graphics/renderer/Renderer2D.hpp"
#include "graphics/renderer/textrenderer.hpp"
#include "graphics/text/fontdata/lenka.hpp"
#include "graphics/text/fontdata/consolamono.hpp"
#include "graphics/camera/Camera.hpp"

#include "math/2d/shape.hpp"
#include "graphics/texture/textureatlas.hpp"

#include "graphics/shader/shaders.hpp"
#include "graphics/graphics.hpp"

#include "menu/sample/menuroot.hpp"
#include "hwo_model/trackvisualisation.hpp"

#include "math/2d/LineSegment.hpp"
#include "math/2d/polygon.hpp"
#include "math/2d/polygonTesselator.hpp"
#endif

#include "hwo_model/world.hpp"

#include "ai/testAI.hpp"
#include "ai/jbot.hpp"

#include <vector>
#include <string>

using namespace hwo_protocol;

void botAction(hwo::Controller& controller, hwo::model::World& world) 
{
	controller.think(world, world.getMyCarIndex(), world.getMyCar().controls);
}

bool tickGame(hwo_connection& connection, game_logic& game) {
	boost::system::error_code error = boost::asio::error::eof;
	auto response = connection.receive_response(error);
	
	if (error == boost::asio::error::eof)
	{
		static bool errorShown = false;
		if (!errorShown) 
		{
			std::cout << "Connection closed" << std::endl;
			errorShown = true;
		}
		return false;
	}
	else if (error)
	{
		throw boost::system::system_error(error);
	}
	else {
		auto reaction = game.react(response);
		connection.send_requests(reaction);
		std::cout << "." << std::flush;
	}

	return true;
}


void novisual_run(hwo_connection& connection, const std::string& name, const std::string& key, bool createRace, hwo::Controller& controller) {
	hwo::model::World world;
	hwo::TrackVisualisation trackVisualisation;
	game_logic game(world, trackVisualisation, std::bind(botAction, std::ref(controller), std::ref(world)));

	std::vector<jsoncons::json> joinRequest;
	joinRequest.push_back(make_joinRace(name, key));
	connection.send_requests(joinRequest);

	while(tickGame(connection, game));
}

void run(hwo_connection& connection, const std::string& name, const std::string& key)
{
#ifndef FINAL_BUILD
	std::shared_ptr<hwo::Window> window(new hwo::Window());
	window->createWindow(1280, 720);
	std::shared_ptr<hwo::UserIO> userIO(new hwo::UserIO(window));

	FPS_Manager fps(Timer::time_now(), 60);

	hwo::TextureHandler::getSingleton().createTextures("../textures/textures.dat");

	std::shared_ptr<hwo::Camera> pCamera(new hwo::Camera());
	pCamera->setProjection(0.50f, 2000.f, window->getAspectRatio());

	std::shared_ptr<hwo::Shaders> pShaders(new hwo::Shaders());
	std::shared_ptr<hwo::TextRenderer> pTextRenderer(new hwo::TextRenderer(pShaders, pCamera)); 
	std::shared_ptr<hwo::Renderer2D> pRenderer(new hwo::Renderer2D(pShaders, pCamera));

	std::shared_ptr<hwo::Graphics> pGraphics(new hwo::Graphics(pShaders, pRenderer, pTextRenderer));

	std::shared_ptr<hwo::MenuRoot> menuRoot = std::shared_ptr<hwo::MenuRoot>(new hwo::MenuRoot(window, userIO, "MenuRoot", hwo::vec3<float>(), hwo::vec3<float>(1, 1, 1)));

	{
		vec4<float> carUVLimits = hwo::TextureHandler::getSingleton().textureLimits("Car", vec4<float>(0.5f, 0.5f, 1.0f, 1.0f));
		hwo::Mesh carMesh = hwo::PolygonTesselator<hwo::vec3<float>>().tesselate(Shape::makeBox(1.0f), carUVLimits);
		carMesh.build();
		hwo::MeshCollection::setMesh("Car", carMesh);
	}
#endif

	hwo::model::World world;
	hwo::TrackVisualisation trackVisualisation;
	// HumanController humanController(userIO);
	TestController testController;
	//JBotController testController;

	game_logic game(world, trackVisualisation, std::bind(botAction, std::ref(testController), std::ref(world)));

	std::vector<jsoncons::json> joinRequest;
	
#ifdef FINAL_BUILD
	joinRequest.push_back(make_join(name, key));
	connection.send_requests(joinRequest);
#else
	joinRequest.push_back(make_joinRace(name, key));
	connection.send_requests(joinRequest);
#endif

#ifndef FINAL_BUILD

	hwo::vec3<float> cameraPos(0, 0, 700);
	hwo::vec3<float> mousePos;
	float errorScale = 120;
	float errorWidth = 0.001f;
	float errorOffset = -1.0f;

	// Let's start the show
	while (!window->shouldClose()) {
		if (fps.canTick(Timer::time_now())) {
			float dt = fps.tick(Timer::time_now());

			pRenderer->clearScreen();

			// allow zooming in & out
			cameraPos.z *= (1.0f + userIO->getMouseScroll() * 0.15f);
			bool mouse1Down = userIO->isKeyDown(userIO->getMouseKeyCode(0));

			hwo::vec3<float> mouseNewPos;
			userIO->getCursorPosition(mouseNewPos);

			if(mouse1Down) {
				hwo::vec3<float> mouseDelta = mouseNewPos - mousePos;
				cameraPos -= mouseDelta * cameraPos.z;
			}


			// modifiers for angle error display
			if(userIO->isKeyClicked('W')) {
				errorScale *= 0.9f;
			}
			if(userIO->isKeyClicked('S')) {
				errorScale *= 1.1f;
			}
			if(userIO->isKeyClicked('A')) {
				errorWidth *= 0.9f;
				errorOffset *= 0.9f; // to keep the mid point static
			}
			if(userIO->isKeyClicked('D')) {
				errorWidth *= 1.1f;
				errorOffset *= 1.1f; // to keep the mid point static
			}
			if(userIO->isKeyClicked('Q')) {
				errorOffset += 0.1f;
			}
			if(userIO->isKeyClicked('E')) {
				errorOffset -= 0.1f;
			}


			mousePos = mouseNewPos;

			// world
			if(world.getCars().size() > 0 && trackVisualisation.isReady()) {
				pCamera->setPosition(world.getMyCar().getPos() + cameraPos);
				pRenderer->cameraToGPU();
				trackVisualisation.draw(pGraphics, world, world.getCars());

				
				hwo::model::CarState simulatedState = hwo::model::Simulator::simulateHistory(world);
				trackVisualisation.draw(pGraphics, world, simulatedState, hwo::vec4<float>(0, 1, 0, 0.3f), hwo::model::SwitchDir::Forward);
				simulatedState = hwo::model::Simulator::simulateHistory(world);
				trackVisualisation.draw(pGraphics, world, simulatedState, hwo::vec4<float>(0, 0, 1, 0.5f), hwo::model::SwitchDir::Forward);
			}

			// menus
			pCamera->setPosition(hwo::vec3<float>(0, 0, +1));
			pRenderer->cameraToGPU();

			menuRoot->tick(dt);
			menuRoot->visualise(pGraphics);

			if(world.getCars().size() > 0) {
				std::stringstream ssGas;
				std::stringstream ssVel;
				std::stringstream ssVel2;
				std::stringstream ssAng;
				std::stringstream ssdAng;
				std::stringstream ssMaxVel;
				auto & car = world.getMyCar();
				ssGas << "Gas: ^g" << car.controls.gas;
				ssVel << "Vel: ^g" << car.state.velocity;
				ssVel2 << "safeSlipVel: ^g" << world.physicsModel.guessSafeSpeed(48,world.getTrackPieceRadius(car.state.activeSegment,car.state.activeLane));
				ssAng << "Ang: ^g" << world.getMyCar().state.angle;
				ssdAng << "dAng: ^g" << world.getMyCar().state.deltaAngle;
				
				float maxVelocity = 0;
				if(testController.directions.size() > 0)
					maxVelocity = hwo::model::Simulator::maxCurrentVelocity(world, world.getMyCar(), testController.directions);
				ssMaxVel << "MaxVel: ^g" << maxVelocity;

				pGraphics->m_pTextRenderer->drawText(ssGas.str(), -0.9f, 0.5f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);
				pGraphics->m_pTextRenderer->drawText(ssVel.str(), -0.4f, 0.5f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);
				pGraphics->m_pTextRenderer->drawText(ssVel2.str(), -0.4f, 0.45f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);
				pGraphics->m_pTextRenderer->drawText(ssMaxVel.str(), -0.4f, 0.4f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);
				pGraphics->m_pTextRenderer->drawText(ssAng.str(), +0.1f, 0.5f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);
				pGraphics->m_pTextRenderer->drawText(ssdAng.str(), +0.5f, 0.5f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);

				if(world.getMyCar().state.isTurboAvailable()) {
					pGraphics->m_pTextRenderer->drawText("TURBO AVAILABLE", 0, -0.45f, 0.07f, Color::GOLDEN, hwo::TextRenderer::Align::CENTER, pGraphics->m_fontConsola);
				}
				else if(world.getMyCar().state.isTurboActive(world.gameTick)) {
					pGraphics->m_pTextRenderer->drawText("WROOOOOM!!", 0, -0.45f, 0.07f, Color::GOLDEN, hwo::TextRenderer::Align::CENTER, pGraphics->m_fontConsola);
				}

			}

			{
				hwo::vec3<float> prevPointActual(errorOffset, 0.0f, 0.0f);
				hwo::vec3<float> prevPointPredicted(errorOffset, 0.0f, 0.0f);

				hwo::vec3<float> prevPointActualVelocity(errorOffset, 0.0f, 0.0f);
				hwo::vec3<float> prevPointPredictedVelocity(errorOffset, 0.0f, 0.0f);

				const auto& errors = world.physicsModel.getErrorMetrics();
				for(const auto& error : errors) {
					hwo::vec3<float> nextPointActual(prevPointActual + hwo::vec3<float>(errorWidth, 0, 0));
					hwo::vec3<float> nextPointPredicted(prevPointPredicted + hwo::vec3<float>(errorWidth, 0, 0));
					
					hwo::vec3<float> nextPointActualVelocity(prevPointActualVelocity + hwo::vec3<float>(errorWidth, 0, 0));
					hwo::vec3<float> nextPointPredictedVelocity(prevPointPredictedVelocity + hwo::vec3<float>(errorWidth, 0, 0));

					nextPointActual.y = error.real / errorScale;
					nextPointPredicted.y = error.predicted / errorScale;

					nextPointActualVelocity.y = 10 * error.velocityReal / errorScale;
					nextPointPredictedVelocity.y = 10 * error.velocityPredicted / errorScale;

					pGraphics->m_pRenderer->drawLine(prevPointActual, nextPointActual, 0.01f, Color::GREEN);
					pGraphics->m_pRenderer->drawLine(prevPointPredicted, nextPointPredicted, 0.005f, Color::BLUE);

					// pGraphics->m_pRenderer->drawLine(prevPointActualVelocity, nextPointActualVelocity, 0.01f, Color::RED);
					// pGraphics->m_pRenderer->drawLine(prevPointPredictedVelocity, nextPointPredictedVelocity, 0.005f, Color::ORANGE);

					prevPointActual = nextPointActual;
					prevPointPredicted = nextPointPredicted;

					prevPointActualVelocity = nextPointActualVelocity;
					prevPointPredictedVelocity = nextPointPredictedVelocity;
				}

				std::stringstream ssAvgSqrError;
				ssAvgSqrError << "Angle model avg sqr error: ^g" << world.physicsModel.getAvgSqrError();
				pGraphics->m_pTextRenderer->drawText(ssAvgSqrError.str(), -0.4f, 0.3f, 0.05f, Color::BLUE, hwo::TextRenderer::Align::LEFT, pGraphics->m_fontConsola);

			
			}

			if(testController.evilPlans.size()) {
				if(testController.evilPlans.front().behaviour == TestController::Plan::RAM) {
					pGraphics->m_pTextRenderer->drawText("RAM", 0, -0.4f, 0.09f, Color::GOLDEN, hwo::TextRenderer::Align::CENTER, pGraphics->m_fontConsola);
				}
				else {
					pGraphics->m_pTextRenderer->drawText("DODGE", 0, -0.4f, 0.09f, Color::GOLDEN, hwo::TextRenderer::Align::CENTER, pGraphics->m_fontConsola);
				}
			}
			else {
				pGraphics->m_pTextRenderer->drawText("Cruising", 0, -0.4f, 0.07f, Color::GOLDEN, hwo::TextRenderer::Align::CENTER, pGraphics->m_fontConsola);
			}

			window->swap_buffers();

			userIO->update();
			window->pollEvents();

			tickGame(connection, game);
		}
		else {
			Timer::sleep(1);
		}
	}
#else
	while(tickGame(connection, game));
#endif
}

int main(int argc, const char* argv[])
{
	try
	{
		if (argc != 5)
		{
			std::cerr << "Usage: ./run host port botname botkey" << std::endl;
			return 1;
		}

		const std::string host(argv[1]);
		const std::string port(argv[2]);
		const std::string name(argv[3]);
		const std::string key(argv[4]);
		
		LOG("Host: %s, port: %s, name: %s, key: %s", host.c_str(), port.c_str(), name.c_str(), key.c_str());

		
		
	
#ifndef FINAL_BUILD
		/*
		auto runSlave = [&](const std::string& instanceId, bool createRace, hwo::Controller& controller) {
			hwo_connection slaveConnection(host, port);
			novisual_run(std::ref(slaveConnection), std::string("Sauron").append(instanceId), key, createRace, controller);
		};

		TestController bot2(1.0f);
		// JBotController bot2;
		std::thread s1(runSlave, "_1", false, bot2);
		Timer::sleep(200);
		TestController bot3(1.0f);
		std::thread s2(runSlave, "_2", false, bot3);
		Timer::sleep(200);
		TestController bot4(1.0f);
		std::thread s3(runSlave, "_3", false, bot4);
		Timer::sleep(200);
		*/
		
#endif
		hwo_connection connection(host, port);
		run(connection, name, key);


#ifndef FINAL_BUILD
		
		/*
		s1.join();
		s2.join();
		s3.join();
		*/
		
#endif
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return 2;
	}

	return 0;
}
