
#include "samplebot/protocol.hpp"

namespace hwo_protocol
{

	jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
	{
		jsoncons::json r;
		r["msgType"] = msg_type;
		r["data"] = data;
		return r;
	}

	jsoncons::json make_join(const std::string& name, const std::string& key)
	{
		jsoncons::json data;
		data["name"] = name;
		data["key"] = key;
		return make_request("join", data);
	}

	jsoncons::json make_joinRace(const std::string& name, const std::string& key)
	{
		jsoncons::json data;
		jsoncons::json botId;
		botId["name"] = name;
		botId["key"] = key;

		data["botId"] = botId;

		data["trackName"] = "elaeintarha";
		// data["trackName"] = "imola";
		// data["trackName"] = "keimola";
		// data["trackName"] = "germany";
		// data["trackName"] = "france";
		// data["trackName"] = "usa";
		data["password"] = "123";
		data["carCount"] = 1;

		return make_request("joinRace", data);
	}

	jsoncons::json make_ping()
	{
		return make_request("ping", jsoncons::null_type());
	}

	jsoncons::json make_turbo()
	{
		return make_request("turbo", "By the Power of Grayskull...I Have the Power!");
	}


	jsoncons::json make_throttle(double throttle)
	{
		return make_request("throttle", throttle);
	}

	// -1 or +1
	jsoncons::json make_switchLane(hwo::model::SwitchDir direction) {
		std::vector<std::string> options;
		options.push_back("Left");
		options.push_back("Forward");
		options.push_back("Right");
		return make_request("switchLane", options[(direction + 1)]);
	}


}  // namespace hwo_protocol
