#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

#include "hwo_model/world.hpp"
#include "hwo_model/trackvisualisation.hpp"

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic(hwo::model::World& world, hwo::TrackVisualisation& trackVisualisation, std::function<void()> think);
  msg_vector react(const jsoncons::json& msg);
  hwo::model::SwitchDir previousSwitchInstruction;
  
private:
  std::function<void()> think;
  typedef std::function<msg_vector(const jsoncons::json&)> action_fun;
  std::map<std::string, action_fun> action_map;
  bool noReply;

  float prevThrottle;
  hwo::model::SwitchDir prevLaneSwitch;

  hwo::model::World& world;
  hwo::TrackVisualisation& trackVisualisation;

  msg_vector game_init(const jsoncons::json& data);
  msg_vector your_car(const jsoncons::json& data);
  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_turbo_start(const jsoncons::json& data);
  msg_vector on_turbo_end(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo(const jsoncons::json& data);
};

#endif
