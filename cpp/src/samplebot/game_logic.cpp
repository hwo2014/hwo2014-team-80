#include "samplebot/game_logic.hpp"
#include "samplebot/protocol.hpp"

#include "hwo_model/switchdir.hpp"
using namespace hwo_protocol;

game_logic::game_logic(hwo::model::World& _world, hwo::TrackVisualisation& visual, std::function<void()> _think) : world(_world), trackVisualisation(visual), think(_think)
{
	action_map["spawn"] = std::bind(&game_logic::on_spawn, this, std::placeholders::_1);
	action_map["join"] = std::bind(&game_logic::on_join, this, std::placeholders::_1);
	action_map["yourCar"] = std::bind(&game_logic::your_car, this, std::placeholders::_1);
	action_map["gameInit"] = std::bind(&game_logic::game_init, this, std::placeholders::_1);
	action_map["gameStart"] = std::bind(&game_logic::on_game_start, this, std::placeholders::_1);
	action_map["carPositions"] = std::bind(&game_logic::on_car_positions, this, std::placeholders::_1);
	action_map["crash"] = std::bind(&game_logic::on_crash, this, std::placeholders::_1);
	action_map["turboStart"] = std::bind(&game_logic::on_turbo_start, this, std::placeholders::_1);
	action_map["turboEnd"] = std::bind(&game_logic::on_turbo_end, this, std::placeholders::_1);
	action_map["gameEnd"] = std::bind(&game_logic::on_game_end, this, std::placeholders::_1);
	action_map["error"] = std::bind(&game_logic::on_error, this, std::placeholders::_1);
	action_map["turboAvailable"] = std::bind(&game_logic::on_turbo, this, std::placeholders::_1);

	prevLaneSwitch = hwo::model::SwitchDir::Forward;
	prevThrottle = 0.0f;
}

template <class T>
inline std::vector<T> toVector(const T& t) {
	std::vector<T> vector;
	vector.push_back(t);
	return vector;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	
	if (msg.has_member("gameTick")) 
	{
		const auto& gameTick = msg["gameTick"];
		world.setGameTick(gameTick.as<int>());
	}
	else {
		noReply = true;
	}

	auto action_it = action_map.find(msg_type);

	if (action_it != action_map.end())
	{
		return action_it->second(data);
	}
	else
	{
		if(msg_type == "tournamentFinished") {
			return toVector(make_ping());
		}

		LOG("Unknown message type: %s", msg_type.c_str());
		LOG("%s", msg.to_string().c_str());
		return game_logic::msg_vector();
	}
}

game_logic::msg_vector game_logic::game_init(const jsoncons::json& data)
{
	std::cout << "Game Init" << std::endl;
	const auto& raceData = data["race"]["track"];
	const auto& carData = data["race"]["cars"];
	const auto& sessionData = data["race"]["raceSession"];
	
	std::string id = raceData["id"].as<std::string>();
	std::string trackName = raceData["name"].as<std::string>();

	{
		world.clearTrack();
		const auto& pieces = raceData["pieces"];
		auto pieces_it = pieces.begin_elements();
		for(;pieces_it != pieces.end_elements(); ++pieces_it) {
			if(pieces_it->has_member("length")) {
				// straight piece
				float length = static_cast<float>((*pieces_it)["length"].as<double>());
				bool hasSwitch = false;

				if(pieces_it->has_member("switch")) {
					hasSwitch = (*pieces_it)["switch"].as<bool>();
					ASSERT(hasSwitch, "ok makes no sense");
				}

				hwo::model::TrackPiece trackPiece;
				trackPiece.straightLength = length;
				trackPiece.hasSwitch = hasSwitch;
				world.addTrackLine(trackPiece);
			}
			else {
				float radius = static_cast<float>((*pieces_it)["radius"].as<int>());
				float angle  = static_cast<float>((*pieces_it)["angle"].as<double>());
				bool hasSwitch = false;

				if(pieces_it->has_member("switch")) {
					hasSwitch = (*pieces_it)["switch"].as<bool>();
				}

				hwo::model::TrackPiece trackPiece;
				trackPiece.arcedAngle = -angle * hwo::math::PI / 180.0f;
				trackPiece.arcedRadius = radius;
				trackPiece.hasSwitch = hasSwitch;
				world.addTrackLine(trackPiece);
			}
		}
	}

	// starting point world coordinates are useless, no need to read.
	/*
	{
	const auto& startingPoint = raceData["startingPoint"];

	}
	*/

	{
		const auto& lanes = raceData["lanes"];
		auto lanes_it = lanes.begin_elements();
		std::vector<float> laneDistances;
		for(;lanes_it != lanes.end_elements(); ++lanes_it) {
			float laneDistance = static_cast<float>((*lanes_it)["distanceFromCenter"].as<double>());
			int index = (*lanes_it)["index"].as<int>();
			laneDistances.push_back(laneDistance);

			LOG("lanedist[%d]: %f", index, laneDistance);
			// TODO: Verify that index behaves well.
		}

		world.setLaneOffsets(laneDistances);

#ifndef FINAL_BUILD
		trackVisualisation.buildVisualTrack(laneDistances, world.getTrack());
#endif
	}


	{
		world.clearCars();
		for(auto it = carData.begin_elements(); it != carData.end_elements(); ++it) {
			std::string carName = (*it)["id"]["name"].as<std::string>();
			std::string carColor = (*it)["id"]["color"].as<std::string>();

			float carLength = static_cast<float>((*it)["dimensions"]["length"].as<double>());
			float carWidth = static_cast<float>((*it)["dimensions"]["width"].as<double>());
			float guideFlagPosition = static_cast<float>((*it)["dimensions"]["guideFlagPosition"].as<double>());

			world.addCar(hwo::model::Car(carName, carColor, hwo::vec3<float>(carLength, carWidth, 0)));
		}
	}

	{
		int laps = 100;
		if(sessionData.has_member("laps"))
			laps = sessionData["laps"].as<int>();
		int maxLapTimeMs = 60000;
		if(sessionData.has_member("maxLapTimeMs"))
			maxLapTimeMs =  sessionData["maxLapTimeMs"].as<int>();
		bool quickRace = false;
		if(sessionData.has_member("quickRace"))
			quickRace = sessionData["quickRace"].as<bool>();
		world.sessionInfo.set(laps, maxLapTimeMs, quickRace);
	}

	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::your_car(const jsoncons::json& data)
{
	LOG("Your Car");
	std::string carColor = data["color"].as<std::string>();
	std::string carName = data["name"].as<std::string>();
	world.setMyCar(carName, carColor);
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	LOG("Joined");
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	LOG("Race started");
	return toVector(make_ping());
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	for(auto it = data.begin_elements(); it != data.end_elements(); ++it) {
		const auto& carID = (*it)["id"]; // {name : ..., color : ...}
		std::string carName = carID["name"].as<std::string>();
		std::string carColor = carID["color"].as<std::string>();

		const auto& piecePosition = (*it)["piecePosition"];
		float angle = static_cast<float>((*it)["angle"].as<double>());

		const auto& lane = piecePosition["lane"];
		int startLaneIndex = lane["startLaneIndex"].as<int>();
		int endLaneIndex = lane["endLaneIndex"].as<int>();
		int lap = piecePosition["lap"].as<int>();

		int pieceIndex = piecePosition["pieceIndex"].as<int>();
		float inPieceDistance = static_cast<float>(piecePosition["inPieceDistance"].as<double>());
		if(world.getTrack().size() > 0) {
			auto & offsets = world.getLaneOffsets();
			
			if(inPieceDistance > world.getTrack()[pieceIndex].getLength(offsets, endLaneIndex)) {
				LOG("InPieceDistance: %f, pieceLength: %f", inPieceDistance, world.getTrack()[pieceIndex].getLength(offsets, endLaneIndex));
				LOG("piece angle: %f, piece radius: %f", world.getTrack()[pieceIndex].arcedAngle, world.getTrack()[pieceIndex].arcedRadius);
			}
			// inPieceDistance /= world.getTrack()[pieceIndex].getLength(offSet);
		}

		world.updateCar(carColor, startLaneIndex, endLaneIndex, pieceIndex, inPieceDistance, angle, lap);
	}
	world.allCarsUpdated();

	think();
	
	auto & controls = world.getControls();

	if (controls.badModel) 
	{
		controls.badModel = false;
		world.physicsModel.recalculate();
	}

	float throttle = controls.gas;
	
	if(controls.turboBoost) 
	{
		controls.turboBoost = false;
		if (world.getMyCar().state.isTurboAvailable()) 
		{
			LOG("TURBO!!!!!");
			world.getMyCar().state.activateTurbo(world.gameTick);
			return toVector(make_turbo());
		}
	}
	
	auto laneSwitch = world.getSwitchLane();
	if(laneSwitch != hwo::model::SwitchDir::Forward && laneSwitch != previousSwitchInstruction) {
		previousSwitchInstruction = laneSwitch;
		return toVector(make_switchLane(laneSwitch));
	}
	previousSwitchInstruction = laneSwitch;

	prevThrottle = throttle;

	if(noReply) {
		noReply = false;
		return game_logic::msg_vector();
	}
	return toVector(make_throttle(throttle));
}

game_logic::msg_vector game_logic::on_turbo(const jsoncons::json& data)
{
	
	float durationMs = static_cast<float>( data["turboDurationMilliseconds"].as<double>());
	int durationTicks = data["turboDurationTicks"].as<int>();
	float turboFactor = static_cast<float>( data["turboFactor"].as<double>());

	LOG("Turbo!! Ticks %d, factor %f", durationTicks, turboFactor);
	world.setTurboSettings(durationMs, durationTicks, turboFactor);
	
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::string carColor = data["color"].as<std::string>();
	world.getCar(carColor).state.dead = true;
	if(world.myCar.carColor == carColor) {
		world.physicsModel.addSample_Death();
		LOG("I crashed :((");
	}
	else {
		LOG("Someone else crashed :))");
	}
	
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data)
{
	std::string carColor = data["color"].as<std::string>();
	world.getCar(carColor).state.turboStartMessage(world.gameTick);
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data)
{
	std::string carColor = data["color"].as<std::string>();
	world.getCar(carColor).state.turboEndMessage(world.gameTick);
	return game_logic::msg_vector();
}



game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data) {
	LOG("spawn");
	world.getCar(data["color"].as<std::string>()).state.dead = false;
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	LOG("Race ended");
	return game_logic::msg_vector();
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	LOG("Error: %s", data.to_string().c_str());
	return game_logic::msg_vector();
}
