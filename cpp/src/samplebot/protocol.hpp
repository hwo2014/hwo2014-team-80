#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include "hwo_model/switchdir.hpp"
namespace hwo_protocol
{
  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data);
  jsoncons::json make_join(const std::string& name, const std::string& key);

  jsoncons::json make_createRace(const std::string& name, const std::string& key);
  jsoncons::json make_joinRace(const std::string& name, const std::string& key);

  jsoncons::json make_ping();
  jsoncons::json make_turbo();
  jsoncons::json make_throttle(double throttle);
  jsoncons::json make_switchLane(hwo::model::SwitchDir direction);
}

#endif
