
#pragma once

#ifndef FINAL_BUILD
#include "math/2d/LineSegment.hpp"
#endif

#include "util/vec3.hpp"
#include "util/vec4.hpp"
#include "graphics/color.hpp"
#include <vector>
#include <memory>

namespace hwo {
	namespace model {
		struct TrackPiece;
		class Car;
		class World;
		class CarState;
#ifndef FINAL_BUILD
		enum SwitchDir;
#endif
	}

#ifndef FINAL_BUILD
	class Graphics;
#endif
}

namespace hwo {
class TrackVisualisation {

	static const int numSegments = 20;

#ifndef FINAL_BUILD
	struct VisualTrackPiece {
		std::vector<hwo::LineSegment<hwo::vec3<float>>> lines;
		hwo::vec4<float> color;
	};

	struct VisualTrackLane {
		std::vector<VisualTrackPiece> lanePieces;
	};

	std::vector<VisualTrackLane> visualTrack;
	// std::vector<hwo::vec3<float>> carPositions; // TODO
#endif

public:
	bool isReady() {
#ifndef FINAL_BUILD
		return visualTrack.size() > 0;
#else
		return true;
#endif
	}

	void buildVisualTrack(const std::vector<float>& lineOffsets, const std::vector<hwo::model::TrackPiece>& track);
#ifndef FINAL_BUILD
	void draw(std::shared_ptr<hwo::Graphics> pGraphics, const hwo::model::World& world, const hwo::model::CarState& car, const vec4<float>& carColor, hwo::model::SwitchDir direction) const;
	void draw(std::shared_ptr<hwo::Graphics> pGraphics, const hwo::model::World& world, const std::vector<hwo::model::Car>& cars) const;
#endif
};
}
