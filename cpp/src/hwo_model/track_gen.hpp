
#pragma once

#include "hwo_model/world.hpp"
#include "math/2d/LineSegment.hpp"
#include "util/vec3.hpp"

namespace TrackGen {
	static void testTrack(hwo::model::World& world) {
		hwo::model::TrackPiece straigth1;
		straigth1.straightLength = 1.0f;
		hwo::model::TrackPiece arced90;
		arced90.arcedRadius = 1.0f;
		arced90.arcedAngle = hwo::math::PI * 0.5f;
		
		hwo::model::TrackPiece counterArced90;
		counterArced90.arcedRadius = 4.0f;
		counterArced90.arcedAngle = -hwo::math::PI * 0.5f;

		world.addTrackLine(straigth1);
		world.addTrackLine(arced90);
		world.addTrackLine(counterArced90);
		world.addTrackLine(arced90);
		world.addTrackLine(arced90);
		world.addTrackLine(counterArced90);
		world.addTrackLine(arced90);
		world.addTrackLine(straigth1);
		world.addTrackLine(arced90);
		world.addTrackLine(counterArced90);
		world.addTrackLine(arced90);
		world.addTrackLine(arced90);
		world.addTrackLine(counterArced90);
		world.addTrackLine(arced90);

		std::vector<float> offsets;
		offsets.push_back(-0.04f * 2);
		offsets.push_back(+0.00f * 2);
		offsets.push_back(+0.04f * 2);
		world.setLaneOffsets(offsets);
	}
}