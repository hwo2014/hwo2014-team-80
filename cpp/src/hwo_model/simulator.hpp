
#pragma once

#include "hwo_model/switchdir.hpp"
#include <vector>

namespace hwo {
	namespace model {
		class World;
		class CarState;
		class Car;
		struct LaneChoice;

		namespace Simulator {
			bool simulateOneTick(const hwo::model::World& world, CarState& car, hwo::model::SwitchDir& nextDirection, float gasInput, int tick);
			int simulateTimeForPath(const hwo::model::World& world, CarState& car, const std::vector<hwo::model::SwitchDir>& path);

			bool isSafeThrottle(const hwo::model::World& world, const CarState& car, hwo::model::SwitchDir nextDirection, float throttle);
			float getSafeThrottle(const hwo::model::World& world, const CarState& car, hwo::model::SwitchDir nextDirection);

			float maxCurrentVelocity(const hwo::model::World& world, const hwo::model::Car& car, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions);

			float getThrottleForVelocity(const hwo::model::World& world, const CarState& car, float targetVelocity, float targetDistance);

			hwo::model::CarState simulateHistory(const hwo::model::World& world);
		}
	}
}
