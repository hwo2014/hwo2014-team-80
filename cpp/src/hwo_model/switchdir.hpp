
#pragma once

namespace hwo {
	namespace model {
		enum SwitchDir
		{
			Left = -1,
			Forward = 0,
			Right = 1,
		};
	}
}
