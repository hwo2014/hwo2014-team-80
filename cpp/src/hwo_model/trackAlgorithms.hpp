
#pragma once

#include "hwo_model/car.hpp"
#include "hwo_model/switchdir.hpp"
#include "hwo_model/trackPiece.hpp"

#include <limits>
#include <string>

namespace hwo {
	namespace model {

		struct LaneChoice {
			LaneChoice() : switchDirection(SwitchDir::Forward), distanceToEnd(std::numeric_limits<float>::max()) {}
			SwitchDir switchDirection;
			float distanceToEnd;
		};

		struct NearestEnemy {
			CarState enemy;
			std::string colorName;
			float distance;
		};

		class World;
		struct LaneChoice;

		namespace TrackAlgorithms {
			// We return a copy as this function is meant to be run only once per track, all useful info of the track should be in the return matrix.
			std::vector<std::vector<LaneChoice>> buildShortestPaths(const std::vector<float>& laneOffsets, const std::vector<hwo::model::TrackPiece>& track);
			int longestStraight(const hwo::model::World& world);
			NearestEnemy distanceToNextHostile(const hwo::model::World& world, int lane);
			NearestEnemy distanceToNextHostile(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions);
			float distanceToNextBend(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions);
			float distanceToNextSwitch(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions);
		}
	}
}