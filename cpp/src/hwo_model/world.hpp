
#pragma once

#include "hwo_model/car.hpp"
#include "hwo_model/physicsModel.hpp"

#include "util/logging.hpp"
#include "hwo_model/switchdir.hpp"
#include "hwo_model/trackPiece.hpp"
#include "hwo_model/trackAlgorithms.hpp"

#include <vector>
#include <queue>
#include <sstream>
#include <string>

namespace hwo {
	namespace model {

		

		class SessionInfo 
		{
		public:
			int laps;
			int maxLapTime;
			bool quickRace;
			SessionInfo() : laps(0), maxLapTime(0), quickRace(false) {}

			void set(int _laps, int _maxLapTimeMs, bool _quickRace) 
			{
				laps = _laps;
				maxLapTime = _maxLapTimeMs;
				quickRace = _quickRace;
			}
		}; 

		class World {
		public:
			struct MyCar {
				std::string carName;
				std::string carColor;
			};
			
			hwo::model::Car::Controls emptyControls;
			
			//
			MyCar myCar;
			int gameTick;
			World() : gameTick(0) {}

			void setGameTick(int tick) 
			{
				gameTick = tick;
			}

			void setTurboSettings(float durationMs, int durationTicks, float turboFactor) 
			{
				TurboSettings turboSettings;
				turboSettings.announcedOnTick = gameTick;
				turboSettings.durationTicks = durationTicks;
				turboSettings.turboFactor = turboFactor;

				for (unsigned i = 0; i < cars.size(); i++) 
				{
					cars[i].state.setTurboAvailable(turboSettings);
				}
			}

			SessionInfo sessionInfo;
			std::vector<float> trackOffsets;
			std::vector<TrackPiece> track;
			std::vector<hwo::model::Car> cars;
			
			hwo::model::Physics physicsModel;

		public:

			void setMyCar(const std::string& carName, const std::string& carColor) {
				myCar.carName = carName;
				myCar.carColor = carColor;
			}

			int getMyCarIndex() const {
				for(unsigned i = 0; i < cars.size(); ++i) {
					const auto& car = cars[i];
					if(car.colorName == myCar.carColor) {
						return i;
					}
				}

				return -1;
			}

			SwitchDir getSwitchLane() {
				if(cars.size() > 0)
					return getMyCar().controls.switchLane;
				return hwo::model::SwitchDir::Forward;
			}
			
			float getNearCollisionDistance(int lane, const CarState& state1, const CarState& state2) 
			{
				int pieceIndex1 = state1.activeSegment;
				int pieceIndex2 = state2.activeSegment;

				if (pieceIndex1 == pieceIndex2) 
				{
					return std::abs(state1.activeSegmentPosition - state2.activeSegmentPosition);
				}
				if ( (pieceIndex1+1) % track.size() == pieceIndex2) 
				{
					return getTrackPieceLength(pieceIndex1, lane)-state1.activeSegmentPosition+state2.activeSegmentPosition;
				}

				if ( (pieceIndex2+1) % track.size() == pieceIndex1) 
				{
					return getTrackPieceLength(pieceIndex2, lane)-state2.activeSegmentPosition+state1.activeSegmentPosition;
				}

				return 1000; //don't care

			}
			void allCarsUpdated() 
			{
				//ignore collision samples

				auto & myCar = getMyCar();
				auto & myCarState = myCar.state;

				float carLength = myCar.dimensions[0];
				
				for (unsigned i = 0; i < cars.size(); i++) 
				{
					if (cars[i].colorName == myCar.colorName)  
					{
						continue;
					}

					if (cars[i].state.activeLane == myCarState.activeLane) 
					{
						float distance = getNearCollisionDistance(cars[i].state.activeLane, cars[i].state, myCarState);

						float limit = carLength * 1.5f;
						if (distance < limit) 
						{
							physicsModel.invalidateSample_Collision();
						}
					}

				}
			}

			void updateCar(const std::string& carColor, int startLaneIndex, int endLaneIndex, int pieceIndex, float inPieceDistance, float angle, int lap) {

				auto& carObject = getCar(carColor);
				auto& car = carObject.state;
				if(car.dead)
					return;
				
				car.lap = lap;
				if(track.size() > 0) {
					float diff = inPieceDistance - car.activeSegmentPosition;
					if(car.activeSegment != pieceIndex) {
						float length = getTrackPieceLength(car.activeSegment, car.activeLane);
						diff += length;
					}

					float length = getTrackPieceLength(pieceIndex, startLaneIndex);
					float movementPerTick = diff;
					float acceleration = movementPerTick - car.velocity;
					float deltaAngle = angle - car.angle;
					float deltaDeltaAngle = deltaAngle - car.deltaAngle;

					ASSERT(startLaneIndex >= 0 && startLaneIndex < 3, "oh noes");

					if(carColor == myCar.carColor) {
						physicsModel.addSample(
							carObject.controls.gas,
							getTrackPieceRadius(car.activeSegment, car.activeLane),
							car.velocity,
							car.acceleration,
							car.angle,
							car.deltaAngle,
							car.deltaDeltaAngle,
							movementPerTick,
							acceleration,
							angle,
							deltaAngle,
							deltaDeltaAngle,
							getTrack()[car.activeSegment].arcedAngle,
							car.getTurboFactor(gameTick),
							getTrack()[car.activeSegment].hasSwitch,
							startLaneIndex,
							pieceIndex,
							inPieceDistance
						);
					}

					car.deltaAngle = deltaAngle;
					car.deltaDeltaAngle = deltaDeltaAngle;
					
					car.velocity = movementPerTick;
					car.acceleration = acceleration;
					carObject.stats.addVelocitySample(pieceIndex, movementPerTick);
				}

				car.activeLane = startLaneIndex;
				car.switchingToLane = endLaneIndex;
				car.angle = angle;
				car.activeSegment = pieceIndex;
				car.activeSegmentPosition = inPieceDistance;
			}

			// TODO: Take current switch into account.
			float getTrackPieceRadius(int pieceIndex, int laneIndex, SwitchDir currentSwitch = SwitchDir::Forward) const {
				if(track[pieceIndex].isStraight())
					return 0;
				return track[pieceIndex].arcedRadius + trackOffsets[laneIndex] * (track[pieceIndex].arcedAngle > 0 ? +1 : -1);
			}

			float getTrackPieceLength(int pieceIndex, int laneIndex, SwitchDir currentSwitch = SwitchDir::Forward) const {
				return track[pieceIndex].getLength(trackOffsets, laneIndex, currentSwitch);
			}

			hwo::model::Car::Controls& getControls() 
			{
				if(cars.size() > 0)
					return getMyCar().controls;
				return emptyControls;
			}
			
			void setLaneOffsets(const std::vector<float>& offsets) {
				trackOffsets = offsets;
			}

			const std::vector<float>& getLaneOffsets() const {
				return trackOffsets;
			}

			void clearTrack() {
				track.clear();
				physicsModel.addSample_Death();
			}

			const std::vector<TrackPiece>& getTrack() const {
				return track;
			}

			void clearCars() {
				cars.clear();
			}

			const std::vector<Car>& getCars() const {
				return cars;
			}

			const hwo::model::Car& getCar(int index) const {
				return cars[index];
			}

			hwo::model::Car& getCar(int index) {
				return cars[index];
			}

			hwo::model::Car& getCar(const std::string& color) {
				for(Car& car : cars) {
					if(car.colorName == color) {
						return car;
					}
				}

				ASSERT(false, "nono");
				return cars.back();
			}

			const hwo::model::Car& getCar(const std::string& color) const {
				for(const Car& car : cars) {
					if(car.colorName == color) {
						return car;
					}
				}

				ASSERT(false, "nono");
				return cars.back();
			}

			hwo::model::Car& getMyCar() {
				return getCar(myCar.carColor);
			}

			const hwo::model::Car& getMyCar() const {
				return getCar(myCar.carColor);
			}

			void addTrackLine(const TrackPiece& trackLine) {
				track.push_back(trackLine);
			}

			void addCar(const Car& car) {
				cars.push_back(car);
			}

		};
	}
}
