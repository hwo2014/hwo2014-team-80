
#include "hwo_model/trackvisualisation.hpp"
#include "hwo_model/world.hpp"

#ifndef FINAL_BUILD
#include "math/matrix/matrix4.hpp"
#endif

void hwo::TrackVisualisation::buildVisualTrack(const std::vector<float>& lineOffsets, const std::vector<hwo::model::TrackPiece>& track) {
#ifndef FINAL_BUILD
	visualTrack.clear();
		
	hwo::vec3<float> currentPos;
	float angle = 0;

	for(unsigned lineID=0; lineID<lineOffsets.size(); ++lineID) {
		visualTrack.push_back(VisualTrackLane());
		auto& currentLane = visualTrack.back();
		for(const hwo::model::TrackPiece& piece : track) {
			float x_offset = hwo::math::sin(angle);
			float y_offset = -hwo::math::cos(angle);

			VisualTrackPiece vpiece;
			if(piece.isStraight()) {
				vpiece.color = Color::GREEN;
				if(piece.hasSwitch)
					vpiece.color = Color::LIGHT_RED;

				for(int i=0; i<numSegments; ++i) {
					float start = i * piece.straightLength / static_cast<float>(numSegments);
					float end   = (i+1) * piece.straightLength / static_cast<float>(numSegments);
					float start_x = hwo::math::cos(angle) * start;
					float start_y = hwo::math::sin(angle) * start;
					float end_x = hwo::math::cos(angle) * end;
					float end_y = hwo::math::sin(angle) * end;
					vpiece.lines.push_back(hwo::LineSegment<hwo::vec3<float>>(
						hwo::vec3<float>(currentPos.x + start_x + lineOffsets[lineID] * x_offset, currentPos.y + start_y + lineOffsets[lineID] * y_offset, 0),
						hwo::vec3<float>(currentPos.x + end_x + lineOffsets[lineID] * x_offset, currentPos.y + end_y + lineOffsets[lineID] * y_offset, 0)
					));
				}

				currentPos += hwo::vec3<float>(hwo::math::cos(angle) * piece.straightLength, hwo::math::sin(angle) * piece.straightLength, 0);
			}
			else {
				vpiece.color = Color::RED;
				hwo::vec3<float> pos = currentPos;

				int innerCircle = (piece.arcedAngle > 0 ? +1 : +1);
				float laneLength = 0;

				for(int i=0; i<numSegments; ++i) {
					float prevAngle = angle + (i+0) * piece.arcedAngle / static_cast<float>(numSegments);
					float nextAngle = angle + (i+1) * piece.arcedAngle / static_cast<float>(numSegments);
					float length = piece.arcedRadius * std::abs(piece.arcedAngle) / static_cast<float>(numSegments);
								
					float x_offset_start = hwo::math::sin(prevAngle) * lineOffsets[lineID] * innerCircle;
					float y_offset_start = -hwo::math::cos(prevAngle) * lineOffsets[lineID] * innerCircle;
								
					float x_offset_end = hwo::math::sin(nextAngle) * lineOffsets[lineID] * innerCircle;
					float y_offset_end = -hwo::math::cos(nextAngle) * lineOffsets[lineID] * innerCircle;

					float start_x = pos.x;
					float start_y = pos.y;

					float end_x = hwo::math::cos(nextAngle) * length + pos.x;
					float end_y = hwo::math::sin(nextAngle) * length + pos.y;
					vpiece.lines.push_back(hwo::LineSegment<hwo::vec3<float>>(
						hwo::vec3<float>(start_x + x_offset_start, start_y + y_offset_start, 0),
						hwo::vec3<float>(end_x + x_offset_end, end_y + y_offset_end, 0)
					));

					laneLength += vpiece.lines.back().lengthApprox();

					pos.x = end_x;
					pos.y = end_y;
				}

				currentPos = pos;
				angle += piece.arcedAngle;
			}

			currentLane.lanePieces.push_back(vpiece);
		}
	}
#endif
}


#ifndef FINAL_BUILD
void hwo::TrackVisualisation::draw(std::shared_ptr<hwo::Graphics> pGraphics, const hwo::model::World& world, const hwo::model::CarState& car, const vec4<float>& carColor, hwo::model::SwitchDir direction) const {
	auto& carLane = visualTrack[car.activeLane];
	auto& carSegment = carLane.lanePieces[car.activeSegment];
		
	float segmentLength = world.getTrackPieceLength(car.activeSegment, car.activeLane);
	float visualPos = (car.activeSegmentPosition * numSegments / segmentLength);
	int activeLineSegment = (int)visualPos;
	float activeLineSegmentProgress = visualPos - activeLineSegment;

	auto& carLineSegment = carSegment.lines[activeLineSegment];
	auto pos = carLineSegment.p1 + (carLineSegment.p2 - carLineSegment.p1) * activeLineSegmentProgress;
	float currentSegmentAngle = ::atan( (carLineSegment.p1.y - carLineSegment.p2.y) / (carLineSegment.p1.x - carLineSegment.p2.x));

	// todo: take into account that car revolves around guide marker, not the center of the object.
	hwo::Matrix4 modelMatrix;
	modelMatrix.makeTranslationMatrix(pos);
	modelMatrix.rotate(-car.angle + currentSegmentAngle * 180.0f / hwo::math::PI, 0, 0, 1); // todo: visualise angle properly
	modelMatrix.scale(hwo::vec3<float>(40, 20, 0));
	pGraphics->m_pRenderer->drawMesh(hwo::MeshCollection::getMesh("Car"), modelMatrix, "Car", carColor);

	vec3<float> trackNormal = carLineSegment.computeNormal().normalize();
	if(direction != 0) {
		pGraphics->m_pRenderer->drawLine(pos, pos + trackNormal * 50 * static_cast<float>(direction), 5.5f, Color::WHITE);
	}
}

void hwo::TrackVisualisation::draw(std::shared_ptr<hwo::Graphics> pGraphics, const hwo::model::World& world, const std::vector<hwo::model::Car>& cars) const {
	for(const auto& trackVisualLane : visualTrack) {
		for(const auto& lanePiece : trackVisualLane.lanePieces) {
			for(const auto& lineSegment : lanePiece.lines) {
				pGraphics->m_pRenderer->drawLine(lineSegment.p1, lineSegment.p2, 5.0f, lanePiece.color);
			}
		}
	}

	for(const hwo::model::Car& car : cars) {
		draw(pGraphics, world, car.state, car.getColor(), car.controls.switchLane);
	}
}
#endif
