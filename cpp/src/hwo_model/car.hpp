
#pragma once

#include "util/vec3.hpp"
#include "util/vec4.hpp"
#ifndef FINAL_BUILD
#include "graphics/graphics.hpp"
#include "math/2d/mesh.hpp"
#include "graphics/meshCollection.hpp"
#endif
#include "switchdir.hpp"
#include <memory>
#include <vector>

namespace hwo {
	namespace model {

		class CarStatistics 
		{
		public:
		
			class PieceStats
			{
			private:
				int samples;
				float sum;
			public:
				PieceStats():samples(0), sum(0) {}

				void addSample(float vel) 
				{
					samples++;
					sum += vel;
				}
				float getAverage() 
				{
					if (samples)
						return sum / samples;
					return 0;
				}
			};

			class RunningAverage 
			{
			private:
				std::vector<float> buffer;
				int index;
				float sum;
			public:
				RunningAverage(int length) : index(0), sum(0)
				{
					ASSERT(length != 0, "Invalid length");
					buffer.resize(length);
					for (unsigned i = 0; i < buffer.size(); i++) 
					{
						buffer[i] = 0; 
					}
				}

				void addSample(float val) 
				{
					sum -= buffer[index];
					sum += val;
					buffer[index] = val;
					index = (index+1) % buffer.size();
				}

				float getAverage() const
				{
					return sum / buffer.size();
				}
			};

		
			
			std::vector<PieceStats> stats;
			RunningAverage			shortTermAverage;
			RunningAverage			midTermAverage;
			RunningAverage			longTermAverage;
			
			CarStatistics() : shortTermAverage(20), midTermAverage(2*60), longTermAverage(15*60)
			{
				
			}

			void addVelocitySample(int pieceIndex, float velocity) 
			{
				if ((int)stats.size() <= pieceIndex) 
				{
					stats.resize(pieceIndex+1);
				}
				stats[pieceIndex].addSample(velocity);
				shortTermAverage.addSample(velocity);
				midTermAverage.addSample(velocity);
				longTermAverage.addSample(velocity);
			}
		};


		class TurboSettings 
		{
		public:
			int announcedOnTick;
			int durationTicks;
			float turboFactor;

			TurboSettings(): announcedOnTick(0), durationTicks(0), turboFactor(1) {}
			
			bool isAvailable() const {return durationTicks != 0;}

		};

		class CarState
		{
		public:
			CarState()
			{
				activeLane = 0;
				switchingToLane = 0;
				activeSegment = 0;
				activeSegmentPosition = 0;
				
				acceleration = 0;
				velocity = 0;

				deltaDeltaAngle = 0;
				deltaAngle = 0;
				angle = 0;
				
				lap = 0;
				turboActivatedTick = -1;
				turboAvailable = false;
				dead = false;
			}

			float acceleration;
			float velocity;

			float deltaDeltaAngle;
			float deltaAngle;
			float angle;

			int lap;

			int activeLane;
			int switchingToLane;
			int activeSegment;
			float activeSegmentPosition;

			bool dead;

		private:
			bool turboAvailable;
			int turboActivatedTick;
			TurboSettings activeTurboSettings;
			TurboSettings availableTurboSettings;
		public:
			
			bool isTurboAvailable() const
			{
				return turboAvailable;
			}

			hwo::model::SwitchDir getCurrentSwitchDir() const {
				return static_cast<hwo::model::SwitchDir>(switchingToLane - activeLane);
			}
			

			void turboStartMessage(int startTick) 
			{
				if (turboAvailable) 
				{
					//for opponent cars, set proper turbo settings
					turboActivatedTick = startTick;	
					activeTurboSettings = availableTurboSettings;
					turboAvailable = false;
				}
				else 
				{
					//for user car fix the tick number if wrong
					turboActivatedTick = startTick;	
				}
			}
			void turboEndMessage(int endTick) 
			{
				turboActivatedTick = -1;
			}

			void activateTurbo(int currentTick) 
			{
				ASSERT(turboAvailable, "Turbo already used");
				if (turboAvailable) 
				{
					turboActivatedTick = currentTick;	
					activeTurboSettings = availableTurboSettings;
					turboAvailable = false;
				}
			}
			
			void setTurboAvailable(TurboSettings & settings) 
			{
				turboAvailable = true;
				availableTurboSettings = settings;
			}

			bool isTurboActive(int currentTick) const
			{
				return turboActivatedTick != -1 && ( (currentTick - turboActivatedTick) <= activeTurboSettings.durationTicks); 
			}
			
			float getTurboFactor(int gameTick) const
			{
				return isTurboActive(gameTick) ? activeTurboSettings.turboFactor : 1.0f;
			}

			const TurboSettings& getAvailableTurbo() const {
				return availableTurboSettings;
			}

			const TurboSettings& getActiveTurbo() const {
				return activeTurboSettings;
			}

			float getRacePosition() const
			{
				return lap*100000.0f
				+activeSegment*1000.0f
				+activeSegmentPosition;
			}
		};

		class Car {			
		public:


			hwo::vec4<float> color;
			hwo::vec3<float> dimensions;
			hwo::vec3<float> pos;

			struct Controls {
				Controls() : gas(0), switchLane(Forward), turboBoost(false), badModel(false) {}
				float gas;
				SwitchDir switchLane;
				bool turboBoost;
				bool badModel;
			};

			Controls controls;
			CarState state;
			CarStatistics stats;
			
			std::string name;
			std::string colorName;

			Car(const std::string& carName, const std::string& carColor, const hwo::vec3<float>& dimensions):state() {
#ifndef FINAL_BUILD
				if(carColor == "red") {
					this->color = Color::RED;
				}
				else if(carColor == "blue") {
					this->color = Color::BLUE;
				}
				else if(carColor == "green") {
					this->color = Color::GREEN;
				}
				else if(carColor == "yellow") {
					this->color = Color::YELLOW;
				}
				else if(carColor == "magenta") {
					this->color = Color::MAGENTA;
				}
				else if(carColor == "orange") {
					this->color = Color::ORANGE;
				}
				else if(carColor == "cyan") {
					this->color = Color::CYAN;
				}
				else {
					this->color = Color::WHITE;
				}
#endif		
				
				name = carName;
				this->colorName = carColor;
				this->dimensions = dimensions;
				
			}

			const hwo::vec3<float>& getPos() const {
				return pos;
			}

			const hwo::vec4<float>& getColor() const {
				return color;
			}

			const hwo::vec3<float>& getDimensions() const {
				return dimensions;
			}
		};
	}
}
