
#pragma once

#include "util/logging.hpp"
#include "math/2d/math.hpp"

#include <vector>
#include <fstream>
#include <functional>

namespace hwo {
	namespace model {

		class Physics {

			struct Sample {
				float gasInput;
				float arcRadius;
				float arcAngle;

				float oldVelocity;
				float oldAcceleration;

				float oldAngle;
				float oldDeltaAngle;
				float oldDeltaDeltaAngle;

				float newVelocity;
				float newAcceleration;

				float newAngle;
				float newDeltaAngle;
				float newDeltaDeltaAngle;

				float activeTurboFactor;
				bool death;
				bool collision;
				bool hasSwitch;

				int activeLane;
				int activeSegment;
				float inSegmentProgress;
			};

			struct Error {
				float predicted;
				float real;

				float velocityPredicted;
				float velocityReal;
			};

			std::vector<Sample> samples;

			mutable std::vector<Error> errors; // only debug info, can be modified in const functions.
			mutable float avgSqrError; // only debug info

			static const int ANGLE_CONSTANT_COUNT = 7;
			float angleConstants[ANGLE_CONSTANT_COUNT];
	
			float engineMaxAcceleration;
			float friction;

			bool frictionInited;
			bool accelerationInited;
			
			int recalculationLimit;

		public:
			Physics() {
				
				recalculationLimit = 10;
				reset();
			}

			const std::vector<Sample>& getSamples() const {
				return samples;
			}

			void reset() {

				engineMaxAcceleration = 0.2f;
				friction = 0.02f;

				frictionInited = false;
				accelerationInited = false;

				float angleInit[ANGLE_CONSTANT_COUNT] = {
					-0.00881533746136f, -0.100391489011f, -2.90589373677f, 0.312507871663f, -0.0662202806157f, -0.00130903067858f
				};

				for (int i = 0; i < ANGLE_CONSTANT_COUNT; i++) {
					angleConstants[i] = angleInit[i];
				}

				avgSqrError = 0;
			}

			const std::vector<Error>& getErrorMetrics() const {
				return errors;
			}

			float getAvgSqrError() const {
				return avgSqrError;
			}

			void addSample(
				float gasInput, float arcRadius,
				float oldVelocity, float oldAcceleration, float oldAngle, float oldDeltaAngle, float oldDeltaDeltaAngle,
				float newVelocity, float newAcceleration, float newAngle, float newDeltaAngle, float newDeltaDeltaAngle,
				float trackAngle, float turboFactor, bool hasSwitch,
				int activeLane, int activeSegment, float inSegmentProgress
			)
			{
				/*
				if(newVelocity == 0 || samples.size() >= 1000) //do not add samples when bot is standing still (finished)
					return;
				*/
				Sample sample;
				sample.gasInput = gasInput;
				sample.arcRadius = arcRadius;

				sample.oldVelocity = oldVelocity;
				sample.oldAcceleration = oldAcceleration;
				sample.oldAngle = oldAngle;
				sample.oldDeltaAngle = oldDeltaAngle;
				sample.oldDeltaDeltaAngle = oldDeltaDeltaAngle;

				sample.newVelocity = newVelocity;
				sample.newAcceleration = newAcceleration;
				sample.newAngle = newAngle;
				sample.newDeltaAngle = newDeltaAngle;
				sample.newDeltaDeltaAngle = newDeltaDeltaAngle;

				sample.arcAngle = trackAngle;
				sample.activeTurboFactor = turboFactor;
				sample.death = false;
				sample.hasSwitch = hasSwitch;
				sample.collision = false;
				
				sample.activeLane = activeLane;
				sample.activeSegment = activeSegment;
				sample.inSegmentProgress = inSegmentProgress;
				
				samples.push_back(sample);
				errors.push_back(Error());

				if(!accelerationInited && newAcceleration > 0.0001f && gasInput > 0.0001f) {
					accelerationInited = true;
					engineMaxAcceleration = newAcceleration / gasInput;
					LOG("Acceleration captured: %f", engineMaxAcceleration);
				}

				if(!frictionInited && newVelocity > 0.0001f && newAcceleration > 0.0001f && gasInput > 0.0001f) {
					frictionInited = true;
					float frictionForce = engineMaxAcceleration * gasInput - newAcceleration;
					friction = frictionForce / newVelocity;
					if(friction < 0.0001f)
						friction = 0.02f;
					LOG("Friction captured: %f", friction);
				}
				
				if(samples.size() == 200) {
					float errorVelocity = optimizeAccelerationFriction();
					float errorAngle = optimizeAngle();
					LOG("angle avg sqr error: %f, avg sqr error velocity: %f", errorAngle, errorVelocity);
					for(int i = 0; i < ANGLE_CONSTANT_COUNT; i++) 
					{
						LOGSNL("\t%d %f", i, angleConstants[i]);
					}
				}
				else if(samples.size() > 200) {
					float errorVelocity = optimizeAccelerationFriction();
					float errorAngle = optimizeAngleSingleStep();
					
					if(samples.size() % 100 == 0) {
						LOG("Angle opt avg sqr error: %f, velocity error: %f", errorAngle, errorVelocity);
						for(int i = 0; i < ANGLE_CONSTANT_COUNT; i++) 
						{
							LOGSNL("\t%d %f", i, angleConstants[i]);
						}
					}
				}
			}

			void recalculate() 
			{
				if (recalculationLimit-- == 0) 
				{
					samples.clear();
					LOG("Samples cleared!");
					recalculationLimit = 10;
				} 
				
				reset();
				float errorVelocity = optimizeAccelerationFriction();
				float errorAngle = optimizeAngle();
				LOG("Model reset: angle avg sqr error: %f, avg sqr error velocity: %f", errorAngle, errorVelocity);
			}

			bool isInited() const
			{
				return samples.size() > 200;
			}

			void addSample_Death() {
				Sample sample;
				sample.oldAcceleration = 0;
				sample.oldVelocity = 0;
				sample.oldAngle = 0;
				sample.oldDeltaAngle = 0;
				sample.oldDeltaDeltaAngle = 0;

				sample.newAcceleration = 0;
				sample.newVelocity = 0;
				sample.newAngle = 0;
				sample.newDeltaAngle = 0;
				sample.newDeltaDeltaAngle = 0;

				sample.activeLane = 0;
				sample.activeSegment = 0;
				sample.inSegmentProgress = 0;
				sample.activeTurboFactor = 1.0f;

				sample.death = true;
				sample.collision = false;
				samples.push_back(sample);
				errors.push_back(Error());
			}

			void invalidateSample_Collision() 
			{
				samples[samples.size()-1].collision = true;
			}

			float guessSafeSpeed(float slipAngle, float radius) const
			{
				const float maxSpeed = 15;
				if (radius == 0)
					return maxSpeed;
				int steps = 100;
				float bestSpeed = 0;
				float minDeltaDelta = 1000;

				for(int i = 0; i <= steps; i++) 
				{
					float speed = maxSpeed * (1- i/ (float)steps);
					if (speed < 1.0f)
						break;
					float deltaDelta = std::abs(guessAngleDeltaDelta(0, 0, speed, radius, 1, -slipAngle, 0, 0));
					if(deltaDelta < minDeltaDelta)
					{
						minDeltaDelta = deltaDelta;
						bestSpeed = speed;
					}
				}
				return bestSpeed;
			}


			float guessAcceleration(float gasInput, float carVelocity, float carAngle, float turboFactor) const {
				return gasInput * engineMaxAcceleration * turboFactor - friction * carVelocity;
			}

			float guessAngleDeltaDelta(float gasInput, float acceleration, float velocity, float arcRadius, float arcAngle, float carAngle, float angleDelta, float angleDeltaDelta) const {
				float directionMultiplier = -1.0f * ((arcAngle<0) ? -1.f : +1.f);
				float curveSqrVelocityLimit = arcRadius * angleConstants[3];
				if(velocity * velocity < curveSqrVelocityLimit) {
					arcRadius = 0;
				}
				
				float dampingForce1 = (carAngle * angleConstants[5] + angleDelta * angleConstants[1]);
				float dampingForce2 = (angleConstants[0] * angleDelta - angleConstants[4] * carAngle) * angleConstants[1] * std::sqrt(std::abs(velocity));
				
				if(arcRadius == 0) {
					return dampingForce1 + dampingForce2;
				}
				
				float slipForce = directionMultiplier * angleConstants[2] * (curveSqrVelocityLimit - velocity * velocity) / arcRadius;
				
				return dampingForce1 + dampingForce2 + slipForce;
			}

			float evaluate() const {
				float sqrErrors = 0;
				float simulatedVelocity = 0;
				float simulatedAcceleration = 0;
				for(unsigned i=1; i<samples.size(); ++i) {
					const Sample& currentSample = samples[i];
					const Sample& prevSample = samples[i-1];

					if(currentSample.death | prevSample.death | prevSample.collision| currentSample.collision) {
						
						errors[i].velocityPredicted = currentSample.newVelocity;
						errors[i].velocityReal = currentSample.newVelocity;

						simulatedAcceleration = currentSample.newAcceleration;
						simulatedVelocity = currentSample.newVelocity;
						
						continue;
					}
					else if(currentSample.hasSwitch | prevSample.hasSwitch ) {
						errors[i].velocityPredicted = simulatedVelocity;
						errors[i].velocityReal = currentSample.newVelocity;

						simulatedAcceleration = currentSample.newAcceleration;
						simulatedVelocity = currentSample.newVelocity;

						continue; // switch pieces are evil, do not learn from them.
					}

					const Sample& sample = prevSample;
					simulatedAcceleration = guessAcceleration(sample.gasInput, simulatedVelocity, sample.oldAngle, sample.activeTurboFactor);
					simulatedVelocity += simulatedAcceleration;
					
					float oneStepEstimatedAcc = guessAcceleration(sample.gasInput, sample.oldVelocity, sample.oldAngle, sample.activeTurboFactor);
					float oneStepEstimatedVel = sample.oldVelocity + simulatedAcceleration;

					errors[i].velocityPredicted = simulatedVelocity;
					errors[i].velocityReal = sample.newVelocity;

					float error = simulatedVelocity - sample.newVelocity;
					//float error = oneStepEstimatedVel - sample.newVelocity;
					sqrErrors += error * error;
				}

				return sqrErrors / samples.size();
			}

			float evaluateAngleModel() const {
				float sqrErrors = 0;
				float sampleW = 0;
				const Sample& firstSample = samples.front();
				float simulatedAngle = 0;
				float simulatedAngleVelocity = 0;
				float simulatedAngleAcceleration = 0;

				for(unsigned i=1; i<samples.size(); ++i) {
					const Sample& currentSample = samples[i];
					const Sample& prevSample = samples[i-1];

					if(currentSample.death | prevSample.death | prevSample.collision| currentSample.collision) {
						errors[i].predicted = currentSample.newAngle;
						errors[i].real = currentSample.newAngle;

						simulatedAngle = currentSample.newAngle;
						simulatedAngleVelocity = currentSample.newVelocity;
						simulatedAngleAcceleration = currentSample.newDeltaDeltaAngle;
						continue;
					}
					else if(currentSample.hasSwitch | prevSample.hasSwitch ) {
						simulatedAngle = currentSample.newAngle;
						simulatedAngleVelocity = currentSample.newVelocity;
						simulatedAngleAcceleration = currentSample.newDeltaDeltaAngle;

						errors[i].predicted = simulatedAngle;
						errors[i].real = currentSample.newAngle;
						continue; // switch pieces are evil, do not learn from them.
					}
					
					const Sample& sample = currentSample;
					simulatedAngleAcceleration = guessAngleDeltaDelta(sample.gasInput, sample.oldAcceleration, sample.oldVelocity, sample.arcRadius, sample.arcAngle, simulatedAngle, simulatedAngleVelocity, simulatedAngleAcceleration);
					simulatedAngleVelocity += simulatedAngleAcceleration;
					simulatedAngle += simulatedAngleVelocity;
					
					float oneStepEstimatedAngleAcceleration = guessAngleDeltaDelta(sample.gasInput, sample.oldAcceleration, sample.oldVelocity, sample.arcRadius, sample.arcAngle, sample.oldAngle,sample.oldDeltaAngle, sample.oldDeltaDeltaAngle);
					float oneStepEstimatedAngleVelocity = sample.oldDeltaAngle + simulatedAngleAcceleration;
					float oneStepEstimatedAngle = sample.oldAngle + simulatedAngleVelocity;
					
					errors[i].predicted = simulatedAngle;
					errors[i].real = sample.newAngle;
					
					float error1 = sample.newAngle - simulatedAngle;
					float error2 = sample.newDeltaAngle - simulatedAngleVelocity;
					float error3 = sample.newDeltaDeltaAngle - simulatedAngleAcceleration;
					sqrErrors += (error1 * error1)*std::abs(sample.newAngle);
					sampleW += std::abs(sample.newAngle);
				}

				avgSqrError = sqrErrors / sampleW;
				return avgSqrError;
			}

			float tweaker(std::function<float ()>& evalFunc,  float bestScore, float& variable, float tweakAmount) {
				while(true) {
					variable += tweakAmount;
					float eval = evalFunc();
					if(eval < bestScore) {
						bestScore = eval;
					}
					else {
						variable -= tweakAmount;
						return bestScore;
					}
				}
			}

			float tweakerSingleStep(std::function<float ()>& evalFunc,float bestScore, float& variable, float tweakAmount) {
				variable += tweakAmount;
				float eval = evalFunc();
				if(eval < bestScore) {
					bestScore = eval;
				}
				else {
					variable -= tweakAmount;
				}

				return bestScore;
			}	
			
			// good estimates for the values are captured on first frames.
			// therefore using relative tweak values makes sense, probably.
			float optimizeAccelerationFriction() {
				float tweakValue = 0.01f;
				float bestScore = evaluate();

				std::function<float()> velocityEval = std::bind(&Physics::evaluate, this);
				bestScore = tweaker(velocityEval, bestScore, engineMaxAcceleration, +tweakValue * engineMaxAcceleration);
				bestScore = tweaker(velocityEval, bestScore, engineMaxAcceleration, -tweakValue * engineMaxAcceleration);
				bestScore = tweaker(velocityEval, bestScore, friction, +tweakValue * friction);
				bestScore = tweaker(velocityEval, bestScore, friction, -tweakValue * friction);
				return bestScore;
			}

			// since we don't know anything about the values at this point,
			// use absolute tweak values to find a good approximation.
			// NOTE: This operation takes a lot (~500ms?) of time.
			float optimizeAngle()
			{
				float tweakValue = 0.01f;
				float bestAngleScore = evaluateAngleModel();
				
				std::function<float()> angleEval = std::bind(&Physics::evaluateAngleModel, this);
				for(int i=0; i<5; ++i) {
					for (int retries = 0; retries < 10; retries++) 
					{ 
						for(int j = 0; j < ANGLE_CONSTANT_COUNT; ++j) 
						{
							bestAngleScore = tweakerSingleStep(angleEval, bestAngleScore, angleConstants[j], +tweakValue);
							bestAngleScore = tweakerSingleStep(angleEval, bestAngleScore, angleConstants[j], -tweakValue);
						}
					}

					tweakValue *= 0.5f;
				}
				tweakValue = 0.01f;
				for(int i=0; i<5; ++i) {
					
					for(int j = 0; j < ANGLE_CONSTANT_COUNT; ++j) 
					{
						bestAngleScore = tweaker(angleEval, bestAngleScore, angleConstants[j], +tweakValue);
						bestAngleScore = tweaker(angleEval, bestAngleScore, angleConstants[j], -tweakValue);
					}
					
					tweakValue *= 0.5f;
				}


				return bestAngleScore;
			}

			float optimizeAngleSingleStep() {
				float bestScore = evaluateAngleModel();
				float prevScore = bestScore;
				std::function<float()> angleEval = std::bind(&Physics::evaluateAngleModel, this);

				for(int j = 0; j < ANGLE_CONSTANT_COUNT; ++j) 
				{
					bestScore = tweaker(angleEval, bestScore, angleConstants[j], +angleConstants[j] * 0.001f);
					bestScore = tweaker(angleEval, bestScore, angleConstants[j], -angleConstants[j] * 0.001f);
				}

				return bestScore;
			}

		};
	}
}
