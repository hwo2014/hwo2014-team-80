
#include "hwo_model/simulator.hpp"
#include "hwo_model/car.hpp"
#include "hwo_model/switchdir.hpp"
#include "hwo_model/world.hpp"

bool hwo::model::Simulator::simulateOneTick(const hwo::model::World& world, hwo::model::CarState& car, hwo::model::SwitchDir& nextDirection, float gasInput, int tick)
{
	float initialAngle = car.angle;
	const auto& physicsModel = world.physicsModel;
	float acc      = physicsModel.guessAcceleration(gasInput, car.velocity, car.angle, car.getTurboFactor(tick));
	auto currentSwitchDirection = static_cast<hwo::model::SwitchDir>(car.switchingToLane - car.activeLane);
	float angleAcc = physicsModel.guessAngleDeltaDelta(
		gasInput,
		car.acceleration,
		car.velocity,
		world.getTrackPieceRadius(car.activeSegment, car.activeLane, currentSwitchDirection),
		world.getTrack()[car.activeSegment].arcedAngle,
		car.angle,
		car.deltaAngle,
		car.deltaDeltaAngle
	);
	
	car.acceleration = acc;
	car.velocity += car.acceleration;
	car.activeSegmentPosition += car.velocity;

	car.deltaDeltaAngle = angleAcc;
	car.deltaAngle += car.deltaDeltaAngle;
	car.angle += car.deltaAngle;

	float pieceLength = world.getTrackPieceLength(car.activeSegment, car.activeLane, currentSwitchDirection);
	
	ASSERT(world.getTrack().size() > 0, "world not initialized");
	const auto& piece = world.getTrack()[car.activeSegment];
	while(car.activeSegmentPosition > pieceLength)
	{
		car.activeSegmentPosition -= pieceLength;
		if(car.activeSegment+1 >= (int)world.getTrack().size()) 
		{
			++car.lap;
		}

		if(world.getTrack()[car.activeSegment].hasSwitch && currentSwitchDirection != hwo::model::Forward) 
		{
			car.activeLane = car.switchingToLane; //complete switch dir
			ASSERT(car.activeLane >= 0 && car.activeLane < (int)world.trackOffsets.size(), "Active lane out of bounds: %d", car.activeLane);
		}

		car.activeSegment = (car.activeSegment+1) % world.getTrack().size();

		if(world.getTrack()[car.activeSegment].hasSwitch) 
		{
			car.switchingToLane = car.activeLane + nextDirection;
			nextDirection = hwo::model::SwitchDir::Forward;
		}
	}

	bool angleTooHigh = ((std::abs(car.angle) > 50) && (car.angle * car.deltaAngle > 0));
	bool tooHighAngularVelocity = ((car.deltaAngle * car.deltaAngle > 4 * 4) && (car.angle * car.deltaAngle > 0));

	car.dead |= angleTooHigh | tooHighAngularVelocity;
	return car.dead;
}


hwo::model::CarState hwo::model::Simulator::simulateHistory(const hwo::model::World& world) {
	hwo::model::CarState state;
	const auto& physicsSamples = world.physicsModel.getSamples();

	if(physicsSamples.size() == 0)
		return state;

	int startIndex = 0;
	while(physicsSamples[startIndex].death) {
		++startIndex;
		if(startIndex >= physicsSamples.size())
			return state;
	}

	auto setState = [&state, &physicsSamples](int sampleIndex) {
		state.activeLane = physicsSamples[sampleIndex].activeLane;
		state.switchingToLane = state.activeLane;
		state.activeSegment = physicsSamples[sampleIndex].activeSegment;
		state.activeSegmentPosition = physicsSamples[sampleIndex].inSegmentProgress;

		state.acceleration = physicsSamples[sampleIndex].newAcceleration;
		state.velocity = physicsSamples[sampleIndex].newVelocity;
		state.angle = physicsSamples[sampleIndex].newAngle;
		state.deltaAngle = physicsSamples[sampleIndex].newDeltaAngle;
		state.deltaDeltaAngle = physicsSamples[sampleIndex].newDeltaDeltaAngle;
	};

	setState(startIndex);

	hwo::model::SwitchDir nextSwitch = hwo::model::SwitchDir::Forward;

	for(size_t i=startIndex; i<physicsSamples.size(); ++i) {
		state.activeLane = physicsSamples[i].activeLane;
		if(physicsSamples[i].death && i+1 < physicsSamples.size()) {
			setState(i+1);
			continue;
		}
		
		ASSERT(physicsSamples[i].activeTurboFactor > 0, "Turbofactor must remain positive");
		simulateOneTick(world, state, nextSwitch, physicsSamples[i].gasInput * physicsSamples[i].activeTurboFactor, i);
	}

	return state;
}


int hwo::model::Simulator::simulateTimeForPath(const hwo::model::World& world, hwo::model::CarState& car, const std::vector<hwo::model::SwitchDir>& path) {
	ASSERT(false, "not implemented");
}

float hwo::model::Simulator::maxCurrentVelocity(const hwo::model::World& world, const hwo::model::Car& car, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions) {
	float min = 0.0f;
	float max = 40.0f;
	
	for(int i=0; i<10; ++i) {
		//LOG("min: %f, max: %f", min, max);
		hwo::model::CarState copy = car.state;
		copy.velocity = (min + max) * 0.5f;
		auto direction = instructions[car.state.activeLane][(car.state.activeSegment + 1) % world.getTrack().size()].switchDirection;
		if(hwo::model::Simulator::isSafeThrottle(world, copy, direction, 0.0f))
			min = (min + max) * 0.5f;
		else
			max = (min + max) * 0.5f;
	}

	return (min + max) * 0.5f;
}

bool hwo::model::Simulator::isSafeThrottle(const hwo::model::World& world, const hwo::model::CarState& car, hwo::model::SwitchDir nextDirection, float throttle) {
	hwo::model::CarState copy = car;
	int tick = world.gameTick;
	for(int i=0; i<5; ++i) {
		simulateOneTick(world, copy, nextDirection, throttle, tick++);
	}
	for(int i=0; i<100; ++i) {
		simulateOneTick(world, copy, nextDirection, 0.0f, tick++);
	}

	return !copy.dead;
}

float hwo::model::Simulator::getSafeThrottle(const hwo::model::World& world, const hwo::model::CarState& car, hwo::model::SwitchDir nextDirection) {
	const int numThrottleSegments = 10;
	for(int i=0; i<numThrottleSegments; ++i) {
		float throttleToTest = 1.0f - i * 1.0f / numThrottleSegments;
		if(isSafeThrottle(world, car, nextDirection, throttleToTest))
			return throttleToTest;
	}
	return 0;
}


float hwo::model::Simulator::getThrottleForVelocity(const hwo::model::World& world, const CarState& car, float targetVelocity, float targetDistance) 
{
	const int steps = 100;

	if(targetDistance <= car.velocity)
	{
		float bestThrottle = 1;
		float minError = 10000*10000;
		for(int i = 0; i < steps; ++i)
		{
			float throttle = 1- i/(float)steps;
			float acc = world.physicsModel.guessAcceleration(throttle, car.velocity, car.angle, car.getTurboFactor(world.gameTick));
			float nextVel = car.velocity + acc;
			float err = nextVel - targetVelocity;
			if(err*err < minError)
			{
				minError = err*err;
				bestThrottle = throttle;
			}
		}
		return bestThrottle;
	}

	for (int i = 0; i < steps; ++i)
	{
		float testThrottle = 1 - i/(float)steps;
		CarState copy = car;
		SwitchDir nextDir = hwo::model::Forward;
		float distance = 0;
		int tick = world.gameTick;

		distance+=car.velocity;
		simulateOneTick(world, copy, nextDir, testThrottle, tick++);

		while (distance < targetDistance && copy.velocity >= 0.1)
		{
			distance+=car.velocity;
			simulateOneTick(world, copy, nextDir, 0, tick++);
			if(distance>= targetDistance  || copy.velocity < 0.1)
			{
				if(copy.velocity < targetVelocity)
				{
					return testThrottle;
				}
				break;
			}
		}
	}
	return 0;
}
