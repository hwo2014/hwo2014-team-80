
#include "hwo_model/trackAlgorithms.hpp"
#include "hwo_model/world.hpp"

std::vector<std::vector<hwo::model::LaneChoice>> hwo::model::TrackAlgorithms::buildShortestPaths(const std::vector<float>& laneOffsets, const std::vector<hwo::model::TrackPiece>& track)
{
	std::vector<std::vector<LaneChoice>> paths;
	
	ASSERT(track.size() > 0, "track not available yet");
	ASSERT(laneOffsets.size() > 0, "lane info not available yet");

	// initialise the pathing matrix for dynamic programming as [numLanes][numTrackPieces]
	paths.resize(laneOffsets.size());
	for(int laneID=0; laneID<static_cast<int>(paths.size()); ++laneID) {
		auto& lane = paths[laneID];
		lane.resize(track.size());
		lane.back().distanceToEnd = track.back().getLength(laneOffsets, laneID, SwitchDir::Forward);
	}

	// define the update function
	auto updatePathInfo = [&](int currentLane, int trackPieceIndex, SwitchDir direction) {
		int prevPieceIndex = (trackPieceIndex == 0) ? track.size()-1 : trackPieceIndex - 1;
		float pieceLength = track[prevPieceIndex].getLength(laneOffsets, currentLane-direction, direction);
		float pathLength = paths[currentLane][trackPieceIndex].distanceToEnd;
		if(pieceLength + pathLength < paths[currentLane-direction][prevPieceIndex].distanceToEnd) {
			paths[currentLane-direction][prevPieceIndex].switchDirection = direction;
			paths[currentLane-direction][prevPieceIndex].distanceToEnd = pieceLength + pathLength;
		}
	};

	// dynamic programming part of the algorithm
	for(int trackPieceIndex = ((int)track.size())-1; trackPieceIndex > 0; --trackPieceIndex) {
		for(int currentLane = 0; currentLane < static_cast<int>(laneOffsets.size()); ++currentLane) {
			LOG("trackPieceIndex: %d, track size: %d, laneIndex %d, number of lanes: %d", trackPieceIndex, (int)track.size(), currentLane, (int)laneOffsets.size());
			if(track[trackPieceIndex-1].hasSwitch) {
				if(currentLane > 0)					                     updatePathInfo(currentLane, trackPieceIndex, SwitchDir::Right);
				if(currentLane < static_cast<int>(laneOffsets.size()-1)) updatePathInfo(currentLane, trackPieceIndex, SwitchDir::Left);
			}
			updatePathInfo(currentLane, trackPieceIndex, SwitchDir::Forward);
		}
	}

	// special case, tie up the pathings
	if(track[track.size()-1].hasSwitch) {
		for(int currentLane = 0; currentLane < static_cast<int>(laneOffsets.size()); ++currentLane) {
			paths[currentLane].back().distanceToEnd = 100000000000.0f;
		}
		for(int currentLane = 0; currentLane < static_cast<int>(laneOffsets.size()); ++currentLane) {
			if(currentLane > 0)					                     updatePathInfo(currentLane, 0, SwitchDir::Right);
			if(currentLane < static_cast<int>(laneOffsets.size()-1)) updatePathInfo(currentLane, 0, SwitchDir::Left);
			updatePathInfo(currentLane, 0, SwitchDir::Forward);
		}
	}

	LOG("%s", "Paths built");

	return paths;
}

hwo::model::NearestEnemy hwo::model::TrackAlgorithms::distanceToNextHostile(const hwo::model::World& world, int lane) {
	const auto& myCar = world.getMyCar();
	const auto& cars = world.getCars();

	hwo::model::NearestEnemy enemy;
	enemy.distance = 10000000000000.0f;

	const auto& track = world.getTrack();
	int trackSize = track.size();

	for(const auto& car : cars) {
		if(myCar.colorName == car.colorName)
			continue;
		if(car.state.dead)
			continue;
		if(car.state.switchingToLane != lane)
			continue;
		
		int otherSegment = car.state.activeSegment;
		int otherLane = car.state.switchingToLane;

		int nextSegment = (myCar.state.activeSegment+1) % trackSize;
		int nextLane = myCar.state.switchingToLane;

		if(otherSegment == myCar.state.activeSegment) {
			// if behind us, don't care.
			if(car.state.activeSegmentPosition < myCar.state.activeSegmentPosition)
				continue;
			
			float dist = car.state.activeSegmentPosition - myCar.state.activeSegmentPosition;
			if(dist < enemy.distance) {
				enemy.distance = dist;
				enemy.enemy = car.state;
				enemy.colorName = car.colorName;
			}
			continue;
		}

		// distance left in my current segment
		float distance = track[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(nextLane - myCar.state.activeLane));
		distance -= myCar.state.activeSegmentPosition;

		while(nextSegment != otherSegment) {
			hwo::model::SwitchDir nextSwitchDir = SwitchDir::Forward;
			distance += track[nextSegment].getLength(world.getLaneOffsets(), nextLane, nextSwitchDir);
			nextLane += nextSwitchDir;
			++nextSegment;
			nextSegment = nextSegment % trackSize;
		}

		distance += car.state.activeSegmentPosition;
		if(distance < enemy.distance) {
			enemy.distance = distance;
			enemy.enemy = car.state;
			enemy.colorName = car.colorName;
		}
	}

	return enemy;
}


hwo::model::NearestEnemy hwo::model::TrackAlgorithms::distanceToNextHostile(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions) {
	const auto& myCar = world.getMyCar();
	const auto& cars = world.getCars();

	hwo::model::NearestEnemy enemy;
	enemy.distance = 10000000000000.0f;

	const auto& track = world.getTrack();
	int trackSize = track.size();

	for(const auto& car : cars) {
		if(myCar.colorName == car.colorName)
			continue;
		if(car.state.dead)
			continue;

		int otherSegment = car.state.activeSegment;
		int otherLane = car.state.switchingToLane;

		int nextSegment = (myCar.state.activeSegment+1) % trackSize;
		int nextLane = myCar.state.switchingToLane;

		if(otherSegment == myCar.state.activeSegment) {
			// if no on our lane, don't care. if behind us, don't care.
			if(car.state.switchingToLane != myCar.state.switchingToLane)
				continue;
			if(car.state.activeSegmentPosition < myCar.state.activeSegmentPosition)
				continue;
			
			float dist = car.state.activeSegmentPosition - myCar.state.activeSegmentPosition;
			if(dist < enemy.distance) {
				enemy.distance = dist;
				enemy.enemy = car.state;
				enemy.colorName = car.colorName;
			}
			continue;
		}

		// distance left in my current segment
		float distance = track[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(nextLane - myCar.state.activeLane));
		distance -= myCar.state.activeSegmentPosition;

		while(nextSegment != otherSegment) {
			hwo::model::SwitchDir nextSwitchDir = instructions[nextLane][nextSegment].switchDirection;
			distance += track[nextSegment].getLength(world.getLaneOffsets(), nextLane, nextSwitchDir);
			nextLane += nextSwitchDir;
			++nextSegment;
			nextSegment = nextSegment % trackSize;
		}

		if(nextLane + instructions[nextLane][nextSegment].switchDirection != otherLane)
			continue;

		distance += car.state.activeSegmentPosition;
		if(distance < enemy.distance) {
			enemy.distance = distance;
			enemy.enemy = car.state;
			enemy.colorName = car.colorName;
		}
	}

	return enemy;
}

int hwo::model::TrackAlgorithms::longestStraight(const hwo::model::World& world) {
	const auto& track = world.getTrack();
	float bestLength = 0;
	int bestIndex = 0;
	for(int i=0; i<(int)track.size(); ++i) {
		float length = 0;
		int index = i;
		while(track[index].isStraight()) {
			length += track[index].getLength(world.getLaneOffsets(), 0);
			++index;
			index = index % track.size();
		}

		if(length > bestLength) {
			bestIndex = i;
			bestLength = length;
		}
	}

	return bestIndex;
}

float hwo::model::TrackAlgorithms::distanceToNextBend(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions) {
	const auto& track = world.getTrack();
	int trackSize = track.size();

	const auto& myCar = world.getMyCar();
	
	if(!track[myCar.state.activeSegment].isStraight())
		return 0;

	// distance left in my current segment
	float distance = track[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(myCar.state.switchingToLane - myCar.state.activeLane));
	distance -= myCar.state.activeSegmentPosition;

	int nextSegment = myCar.state.activeSegment;

	for(int i=0; i<10; ++i) {
		nextSegment = (nextSegment + 1) % trackSize;
		if(track[nextSegment].arcedRadius < 200) {
			return distance;
		}
		distance += track[nextSegment].getLength(world.getLaneOffsets(), myCar.state.switchingToLane);
	}

	return 1000000000.0f;
}

float hwo::model::TrackAlgorithms::distanceToNextSwitch(const hwo::model::World& world, const std::vector<std::vector<hwo::model::LaneChoice>>& instructions) {
	const auto& track = world.getTrack();
	int trackSize = track.size();

	const auto& myCar = world.getMyCar();
	
	// distance left in my current segment
	float distance = track[myCar.state.activeSegment].getLength(world.getLaneOffsets(), myCar.state.activeLane, static_cast<hwo::model::SwitchDir>(myCar.state.switchingToLane - myCar.state.activeLane));
	distance -= myCar.state.activeSegmentPosition;

	int nextSegment = myCar.state.activeSegment;

	for(int i=0; i<10; ++i) {
		nextSegment = (nextSegment + 1) % trackSize;
		if(track[nextSegment].hasSwitch) {
			return distance;
		}
		distance += track[nextSegment].getLength(world.getLaneOffsets(), myCar.state.switchingToLane);
	}

	return 1000000000.0f;
}

