
#pragma once

#include <cmath>
#include <vector>

namespace hwo {
	namespace model {
		struct TrackPiece {
			TrackPiece() {
				straightLength = 0;
				arcedAngle = 0;
				arcedRadius = 0;
				hasSwitch = false;
				isBridge = false;
			}

			bool isStraight() const { return straightLength > 1; }
			bool isArced() const { return !isStraight(); }

			float straightLength;
			float arcedAngle;
			float arcedRadius;
			bool hasSwitch;
			bool isBridge;

			float getLength(const std::vector<float>& trackOffsets, int lane, SwitchDir dir = SwitchDir::Forward) const {
				ASSERT(lane >= 0 && lane < (int)trackOffsets.size(), "Lane out of bounds: %d", lane);
				if(isStraight()) 
				{
					if(hasSwitch) 
					{
						// Note: the hypotenuse of the track end points is not 100% accurate for the length of a switch.
						//       But, it is a pretty good approximation.
						if(dir == SwitchDir::Left && lane > 0) 
						{
							float laneDiff = trackOffsets[lane] - trackOffsets[lane-1];
							return std::sqrt(straightLength*straightLength + laneDiff*laneDiff);
						}
						else if (dir == SwitchDir::Right && lane < (int)trackOffsets.size()-1) 
						{
							float laneDiff = trackOffsets[lane] - trackOffsets[lane+1];
							return std::sqrt(straightLength*straightLength + laneDiff*laneDiff);
						}
					}
					ASSERT(straightLength > 0, "makes no sense");
					return straightLength;
				}

				float laneOffSet = trackOffsets[lane];

				if(hasSwitch) 
				{
					// Note: Not sure what this approximation for the length is.
					if(dir == SwitchDir::Left) 
					{
						ASSERT(lane > 0, "switch dir request not possible");
						float laneDiff = trackOffsets[lane] - trackOffsets[lane-1];
						float r1 = std::abs(arcedAngle) * (arcedRadius + laneOffSet * ((arcedAngle > 0) ? +1 : -1));
						float r2 = std::abs(arcedAngle) * (arcedRadius + trackOffsets[lane-1] * ((arcedAngle > 0) ? +1 : -1));
						return (r1+r2)*0.5f;
					}
					else if(dir == SwitchDir::Right) 
					{
						ASSERT(lane < (int)trackOffsets.size()-1, "switch dir request not possible");
						float laneDiff = trackOffsets[lane] - trackOffsets[lane+1];
						float r1 = std::abs(arcedAngle) * (arcedRadius + laneOffSet * (arcedAngle > 0? +1 : -1));
						float r2 = std::abs(arcedAngle) * (arcedRadius + trackOffsets[lane+1] * (arcedAngle > 0? +1 : -1));
						return (r1+r2)*0.5f;
					}
				}

				float absAngle = std::abs(arcedAngle);
				float modifiedRadius = arcedRadius + laneOffSet * ((arcedAngle > 0) ? +1 : -1);
				return absAngle * modifiedRadius;
			}

			float getRadius(float laneOffSet) const {
				if(isStraight())
					return 10000000.0f;
				return arcedRadius + laneOffSet * (arcedAngle > 0? +1 : -1);
			}
		};
	}
}
