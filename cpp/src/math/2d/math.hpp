
#pragma once

#include <cmath>
#include <utility>

#ifdef max
#undef max
#endif

namespace hwo {
namespace math {

	static const float PI = 3.14159f;

	inline float sin(float f) {
		return ::sin(f);
	}

	inline float cos(float f) {
		return ::cos(f);
	}

	template<class T>
	inline T sqrt(const T& val) {
		return ::sqrt(val);
	}

	template<class T>
	inline T& rotateXY(T& v, float radians) {
		float cosVal = hwo::math::cos(radians);
		float sinVal = hwo::math::sin(radians);
		float xNew = (v.x * cosVal - v.y * sinVal);
		float yNew = (v.x * sinVal + v.y * cosVal);
		v.x = xNew;
		v.y = yNew;
		return v;
	}

	// approximation, good enough
	template<>
	inline float sqrt<float>(const float& val) {
		int* x = (int*)(&val);
        *x = (1 << 29) + (*x >> 1) - (1 << 22) - 0x4C000;
        return *((float*)x);
	}
}

namespace math2D {
	template<class P>
	inline decltype(std::declval<P>().x) distanceSquared(const P& p1, const P& p2) {
		decltype(p1.x) x = (p1.x - p2.x);
		decltype(p1.x) y = (p1.y - p2.y);
		return x * x + y * y;
	}

	template<class P>
	inline bool pointInTriangle(const P& point, const P& t1, const P& t2, const P& t3) {
        P v0, v1, v2;
		v0.x = t3.x - t1.x;
        v0.y = t3.y - t1.y;

        v1.x = t2.x - t1.x;
        v1.y = t2.y - t1.y;

        v2.x = point.x - t1.x;
        v2.y = point.y - t1.y;

        decltype(point.x) dot00 = v0.x * v0.x + v0.y * v0.y;
        decltype(point.x) dot01 = v0.x * v1.x + v0.y * v1.y;
        decltype(point.x) dot02 = v0.x * v2.x + v0.y * v2.y;
        decltype(point.x) dot11 = v1.x * v1.x + v1.y * v1.y;
        decltype(point.x) dot12 = v1.x * v2.x + v1.y * v2.y;

        // Compute barycentric coordinates
        decltype(point.x) invDenom = (decltype(point.x))(1) / (dot00 * dot11 - dot01 * dot01);
        decltype(point.x) u = (dot11 * dot02 - dot01 * dot12) * invDenom;
        decltype(point.x) v = (dot00 * dot12 - dot01 * dot02) * invDenom;

        // Check if point is in triangle
        return (u > 0) && (v > 0) && (u + v < 1);
    }
	
	template<class P>
	inline bool pointLeftOfLine(const P& point, const P& t1, const P& t2) {
		return (t2.x - t1.x) * (point.y - t1.y) - (t2.y - t1.y) * (point.x - t1.x) > (decltype(point.x))(0);
	}

	template<class T>
	inline decltype(std::declval<T>().x) crossProduct(const T& t1, const T& t2) {
        return (t1.x * t2.y - t1.y * t2.x);
    }
}
}
